package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.MenuItem
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.RelativeLayout

class Gyroscope : AppCompatActivity(), SensorEventListener {
    private var gyroanim: ImageView? = null
    private var gyroanimatecircle:ImageView? = null
    internal var r: RotateAnimation? = null
    private var sensorManager: SensorManager? = null
    private var accelerometer: Sensor? = null
    var x: Int = 0
    var y: Int = 0
    internal var heightofanimcirclebox: Int = 0
    internal var widthofanimcirclebox:Int = 0
    private var gyroanimatecirclebox: RelativeLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gyroscope)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

        gyroanimatecirclebox = findViewById(R.id.gyroanimatecirclebox)
        gyroanimatecircle = findViewById(R.id.gyroanimatecircle)

        val displaymetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displaymetrics)
        val w = displaymetrics.widthPixels
        val h = displaymetrics.heightPixels
        widthofanimcirclebox = displaymetrics.widthPixels
        heightofanimcirclebox = displaymetrics.heightPixels
        val squareRoot = Math.sqrt((h * h + w * w).toDouble()).toInt()

        gyroanim = findViewById(R.id.gyroanim)
        gyroanim!!.layoutParams.height = squareRoot
        gyroanim!!.layoutParams.width = squareRoot

        gyroanimatecirclebox!!.post {
            widthofanimcirclebox = gyroanimatecirclebox!!.measuredWidth
            heightofanimcirclebox = gyroanimatecirclebox!!.measuredHeight
        }


        gyroanimatecircle!!.post {
            widthofanimcirclebox -= gyroanimatecircle!!.measuredWidth
            heightofanimcirclebox -= gyroanimatecircle!!.measuredHeight
        }

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometer = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)


        r = RotateAnimation(0f, -360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        r?.duration = 9000.toLong()
        r!!.interpolator = LinearInterpolator()
        r?.repeatCount = Animation.INFINITE
        gyroanim?.startAnimation(r)
    }


    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            val newX = x - event.values[0].toInt()
            val newY = y + event.values[1].toInt()
            if (newX <= widthofanimcirclebox && newX > 0) {
                x = newX
            }
            if (newY <= heightofanimcirclebox && newY > 0) {
                y = newY
            }
            gyroanimatecircle?.x = newX.toFloat()
            gyroanimatecircle?.y = newY.toFloat()
        }
    }

    public override fun onResume() {
        super.onResume()
        //recreate();
        sensorManager?.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME)
    }

    override fun onPause() {
        super.onPause()
        sensorManager?.unregisterListener(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }
}
