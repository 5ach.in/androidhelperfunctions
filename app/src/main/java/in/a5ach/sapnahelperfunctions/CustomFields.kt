package `in`.a5ach.sapnahelperfunctions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.Toast
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView


class CustomFields : AppCompatActivity() {


    var linearListOfFields: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_fields)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

        linearListOfFields = findViewById(R.id.linearListOfFields)

        var loop = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getInt("customFieldsCount", 0))
        for (i in 0 until loop) {
            val params = LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 25, 0, 25)
            val layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val inflatedCustomField = layoutInflater.inflate(R.layout.custom_field, null)
            var customFieldName: TextView = inflatedCustomField.findViewById(R.id.customFieldName)

            customFieldName.text = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getString("customFieldTitle${i+1}", "No name field"))
            var customFieldLinearLayout: LinearLayout = inflatedCustomField.findViewById(R.id.customFieldLinearLayout)


            val paramsForObjectInField = LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            paramsForObjectInField.gravity = Gravity.CENTER_HORIZONTAL
            paramsForObjectInField.setMargins(0, 0, 0, 10)

            val paramsForObjectInField2 = LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            paramsForObjectInField2.gravity = Gravity.CENTER_HORIZONTAL
            paramsForObjectInField2.setMargins(0, 0, 0, 10)

            var positionOfSwitch = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getInt("customFieldPositionOfSwitch${i+1}", 0))
            var valueOfSwitch = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getInt("customFieldValueOfSwitch${i+1}", 0))
            var positionOfEditText = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getInt("customFieldPositionOfEditText${i+1}", 0))
            var valueOfEditText = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getString("customFieldValueOfEditText${i+1}", ""))
            var positionOfSeekBar = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getInt("customFieldPositionOfSeekBar${i+1}", 0))
            var valueOfSeekBar = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getInt("customFieldValueOfSeekBar${i+1}", 0))

            val customFieldMap: Map<View, Int> = mapOf(Switch(this) to positionOfSwitch, EditText(this) to positionOfEditText, SeekBar(this) to positionOfSeekBar)
            //val map = mapOf(Pair(Switch(this), positionOfSwitch), Pair(EditText(this), positionOfEditText), Pair(SeekBar(this), positionOfSeekBar))
            //map.toList().sortedBy {(key, value) -> value}.toMap()

            val map = mutableMapOf<Int, View>()
            if(positionOfSwitch!=0){
                map.computeIfAbsent(positionOfSwitch) {Switch(this)}
            }
            if(positionOfEditText!=0){
                map.computeIfAbsent(positionOfEditText) {EditText(this)}
            }
            if(positionOfSeekBar!=0){
                map.computeIfAbsent(positionOfSeekBar) {SeekBar(this)}
            }

            //val map = mapOf(Pair(positionOfSwitch, Switch(this)), Pair(positionOfEditText, EditText(this)), Pair(positionOfSeekBar, SeekBar(this)))
            map.toSortedMap()
            for (i in 0 until 9) {
                val view = map[i]
                if (view is Switch){
                    view.isChecked = valueOfSwitch != 0
                    customFieldLinearLayout.addView(view, customFieldLinearLayout.childCount, paramsForObjectInField)
                }
                else if (view is EditText){
                    view.setText(valueOfEditText)
                    view.background = null
                    view.hint = "Dummy hint"
                    customFieldLinearLayout.addView(view, customFieldLinearLayout.childCount, paramsForObjectInField2)
                }
                else if (view is SeekBar){
                    view.max = 100
                    view.progress = valueOfSeekBar
                    val seekBarToInflateIndicator = TextView(this)
                    seekBarToInflateIndicator.text = "${valueOfSeekBar}"
                    view.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
                        override fun onStopTrackingTouch(seekBar: SeekBar) {
                        }
                        override fun onStartTrackingTouch(seekBar: SeekBar) {
                        }
                        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                            seekBarToInflateIndicator.text = progress.toString()
                        }
                    })
                    customFieldLinearLayout.addView(view, customFieldLinearLayout.childCount, paramsForObjectInField2)
                    customFieldLinearLayout.addView(seekBarToInflateIndicator, customFieldLinearLayout.childCount, paramsForObjectInField)
                }

            }

            inflatedCustomField.viewTreeObserver.addOnGlobalLayoutListener {
                if (window.decorView.findViewById<View>(android.R.id.content) is ViewGroup) {
                    setDismissKeyboardOnNonEditTexts(window.decorView.findViewById<View>(android.R.id.content) as ViewGroup)
                }
            }
            linearListOfFields?.addView(inflatedCustomField, linearListOfFields!!.childCount, params)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.add -> {
                //startActivityForResult(Intent(baseContext, AddCustomField::class.java), 1)
                val dialogFragment = AddCustomFieldDialogueFragment()

                dialogFragment.show(supportFragmentManager, "addCustomFieldDialogue")

                return true
            }
            R.id.save -> {
                saveCustomFieldValues()
                Toast.makeText(this, "Custom field values saved", Toast.LENGTH_SHORT).show()


                //var customFieldsCount = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getInt("customFieldsCount", 0))+1
                //finish()
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.add_save_menu, menu)
        return true
    }

    private fun setDismissKeyboardOnNonEditTexts(viewGroup: ViewGroup) {
        val count = viewGroup.childCount
        for (i in 0 until count) {
            val view = viewGroup.getChildAt(i)
            if (view is ViewGroup)
                setDismissKeyboardOnNonEditTexts(view)
            else if (view !is EditText) {
                view.setOnTouchListener { _, _ ->
                    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(window.decorView.windowToken, 0)
                    linearListOfFields?.requestFocusFromTouch()
                    false
                }
            }
        }
    }
    fun clearAllCustomFields(v: View) {
        getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().clear().commit()
        Toast.makeText(this, "Cleared all custom fields", Toast.LENGTH_SHORT).show()
        recreate()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 1) {
            recreate()

           /* if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }*/
        }
    }

/*
    public override fun onResume() {
        super.onResume()
        recreate()
    }*/

    fun saveCustomFieldValues() {
        for (i in 0 until linearListOfFields!!.childCount) {

            for (j in 0 until ((linearListOfFields?.getChildAt(i) as ViewGroup).getChildAt(1)as ViewGroup).childCount) {

                if(((linearListOfFields?.getChildAt(i) as ViewGroup).getChildAt(1)as ViewGroup).getChildAt(j).visibility == View.VISIBLE){


                    if (((linearListOfFields?.getChildAt(i) as ViewGroup).getChildAt(1)as ViewGroup).getChildAt(j) is Switch) {
                        val switchView = ((linearListOfFields?.getChildAt(i) as ViewGroup).getChildAt(1)as ViewGroup).getChildAt(j) as Switch
                        if(switchView.isChecked){
                            getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldValueOfSwitch${i+1}", 1).commit()
                        }
                        else{
                            getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldValueOfSwitch${i+1}", 0).commit()
                        }
                    }

                    else if (((linearListOfFields?.getChildAt(i) as ViewGroup).getChildAt(1)as ViewGroup).getChildAt(j) is SeekBar) {
                        val seekBarView = ((linearListOfFields?.getChildAt(i) as ViewGroup).getChildAt(1)as ViewGroup).getChildAt(j) as SeekBar
                        seekBarView.javaClass.getName().toString()

                        getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldValueOfSeekBar${i+1}", seekBarView.progress).commit()
                    }
                    else if (((linearListOfFields?.getChildAt(i) as ViewGroup).getChildAt(1)as ViewGroup).getChildAt(j) is EditText) {
                        val editTextView = ((linearListOfFields?.getChildAt(i) as ViewGroup).getChildAt(1)as ViewGroup).getChildAt(j) as EditText
                        getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putString("customFieldValueOfEditText${i+1}", editTextView.text.toString()).commit()
                    }

                }
            }
        }

    }
}