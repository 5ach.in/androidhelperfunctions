package `in`.a5ach.sapnahelperfunctions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.util.TypedValue
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.*
import java.util.ArrayList
import android.content.ComponentName
import android.graphics.Color
import android.graphics.PorterDuff
import android.support.design.widget.Snackbar
import android.graphics.drawable.GradientDrawable
import android.view.ViewGroup
import android.net.Uri
import java.io.File


class MainActivity : AppCompatActivity() {


    var mainAppBarLayout: AppBarLayout? = null
    var mainToolbar: Toolbar? = null
    var mainInfo: ImageButton? = null
    var mainToolbarSpinner: Spinner? = null
    var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {


        //val listOfActivities = listOf(MainActivity(), TestActivity(), BookPageSlider(), AsyncTaskCountdown(), Animations(), BlurBackground(), CaptureOrUploadImage(), CustomFields(), CustomVibration(), RequestPermissions(), SpeechTextManipulation(), AlarmsReminders(), DrawImage2(), Gyroscope(), RecordAudio(), RegisterLogin())
        val listOfActivities = listOf(MainActivity(), AlarmsReminders(), Animations(), BlurBackground(), BookPageSlider(), CaptureOrUploadImage(), ChangeIconShortcut(), CustomFields(), CustomVibration(), CustomViewTest(), DrawImage(), FloatingActionButtons(), Gyroscope(), PackageManager(), RecordAudio(), RegisterLogin(), RequestPermissions(), SpeechTextManipulation(), ThemeChange(), WidgetCreator())



        super.onCreate(savedInstanceState)
        if (intent.hasExtra("uninstall")) {
            uninstallApp(packageName)
            finish()
        }



        setContentView(R.layout.activity_main)
        mainAppBarLayout = findViewById(R.id.mainAppBarLayout)
        mainToolbar = findViewById(R.id.mainToolbar)
        mainInfo = findViewById(R.id.mainInfo)
        mainInfo?.setOnClickListener{
            val ft = this.fragmentManager.beginTransaction()
            val prev = this.fragmentManager.findFragmentByTag("editTicketDialog")
            if (prev != null) {
                ft.remove(prev)
            }
            ft.addToBackStack(null)
            val dialogFragment = InfoDialogueFragment()
            dialogFragment.show(ft, "editTicketDialog")
        }


        mainToolbarSpinner = findViewById(R.id.mainToolbarSpinner)
        mainToolbar?.title = applicationInfo.loadLabel(this.packageManager)
        val linearListOfButtons: LinearLayout = findViewById(R.id.linearListOfButtons)

        mainToolbar?.setNavigationIcon(R.drawable.ic_delete_forever_black_24dp)
        setSupportActionBar(mainToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        mainToolbar?.setNavigationOnClickListener {
            uninstallApp(packageName)
        }

        snackbar = Snackbar.make(window.decorView.findViewById(android.R.id.content), "", Snackbar.LENGTH_INDEFINITE)
        snackbar?.setAction("Dismiss") {
            snackbar?.dismiss()
        }

        snackbar?.addCallback(object : Snackbar.Callback() {
            override fun onShown(snackbar: Snackbar?) {
                super.onShown(snackbar)
                //snackbar?.dismiss()

            }

            override fun onDismissed(snackbar: Snackbar?, event: Int) {
                super.onDismissed(snackbar, event)
                for (i in 0 until linearListOfButtons.childCount) {
                    val v = (linearListOfButtons!! as ViewGroup).getChildAt(i)
                    if (v is Button) {
                        //v.setBackgroundResource(0)
                        //v.background = null
                        v.getBackground().clearColorFilter()
                        //v.setBackgroundResource(android.R.drawable.btn_default);
                    }
                }
            }
        })

        var openMethod = (getSharedPreferences("MainActivity", Context.MODE_PRIVATE).getInt("OpenMethod", 0))
        if ((getSharedPreferences("MainActivity", Context.MODE_PRIVATE).getInt("OpenMethod", 0)) > listOfActivities.size) {
            openMethod = 0
        }

        var mainFunctions = ArrayList<String>()
        var mainFunctionsDescription = ArrayList<String>()
        for (i in 0 until listOfActivities.size) {
            mainFunctions.add(listOfActivities[i].javaClass.simpleName)
            val cn = ComponentName(this, listOfActivities[i]::class.java)
            val info = packageManager.getActivityInfo(cn, 0)
            mainFunctionsDescription.add(info.nonLocalizedLabel as String)
            //mainFunctionsDescription.add("cub")

        }
        val mainFunctionsAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mainFunctions)
        mainFunctionsAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1)
        mainToolbarSpinner?.adapter = mainFunctionsAdapter
        mainToolbarSpinner?.setSelection(openMethod)

        mainToolbarSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                getSharedPreferences("MainActivity", Context.MODE_PRIVATE).edit().putInt("OpenMethod", position).commit()
            }
        }

        for (i in 0 until listOfActivities.size) {
            val params = LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 25, 0, 25)
            val buttonToInflate = Button(this)
            buttonToInflate.transformationMethod = null
            buttonToInflate.setTextSize(TypedValue.COMPLEX_UNIT_PX, 60.toFloat())
            buttonToInflate.text = listOfActivities[i].javaClass.simpleName
            //buttonToInflate.setBackgroundResource(android.R.drawable.btn_default);

            /* USE THIS CODE FOR INFLATING A CUSTOM VIEW
            val layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val inflatedTicket = layoutInflater.inflate(R.layout.custom_view_you_created, null)
            var button: Button = inflatedTicket.findViewById(R.id.button_inside_the_custom_view)
            button.setText(listOfActivities.get(i).javaClass.simpleName)
            */
            linearListOfButtons.addView(buttonToInflate, linearListOfButtons.childCount, params)
            if (i > 0) {

            }
            buttonToInflate.setOnClickListener {
                if (i > 0) {
                    val intent = Intent(this, listOfActivities[i].javaClass)
                    startActivity(intent)
                }
                snackbar?.dismiss()
            }
            buttonToInflate.setOnLongClickListener {
                for (i in 0 until linearListOfButtons.childCount) {
                    val v = (linearListOfButtons as ViewGroup).getChildAt(i)
                    if (v is Button) {
                        v.getBackground().clearColorFilter()
                    }
                }
                val gd = GradientDrawable()
                gd.setColor(-0xff0100) // Changes this drawbale to use a single color instead of a gradient
                gd.cornerRadius = 5f
                gd.setStroke(1, -0x1000000)
                //buttonToInflate.background = gd

                buttonToInflate.background.setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY)


                //val colorDrawable = ColorDrawable(ContextCompat.getColor(this, R.color.green))
                //buttonToInflate.background = colorDrawable

                //buttonToInflate.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.green))


                //snackbar = Snackbar.make(window.decorView.findViewById(android.R.id.content), "Demonstrates: ${mainFunctionsDescription[i]}", Snackbar.LENGTH_INDEFINITE)
                snackbar?.setText(mainFunctionsDescription[i])
                /*snackbar?.setAction("Dismiss") {
                    snackbar?.dismiss()
                }*/
                snackbar?.show()
                //Toast.makeText(this, "${mainFunctionsDescription[i]}", Toast.LENGTH_LONG).show()
                true
            }
        }

        if (openMethod != 0) {
            val intent = Intent(this, listOfActivities[openMethod].javaClass)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        if (snackbar!!.isShown) {
            snackbar?.dismiss()
        } else {
            setResult(Activity.RESULT_CANCELED)
            super.onBackPressed()
        }
        //Toast.makeText(this, "back", Toast.LENGTH_LONG).show()
    }

    fun uninstallApp(packageName: String?) {
        val intent = Intent(Intent.ACTION_DELETE)
        intent.data = Uri.parse("package:$packageName")
        startActivity(intent)
        /*if(isRootAvailable()){
            val intent = Intent(Intent.ACTION_DELETE)
            intent.data = Uri.parse("package:$packageName")
            startActivity(intent)
        }
        else{

            val intent = Intent(Intent.ACTION_UNINSTALL_PACKAGE)
            intent.data = Uri.parse("package:$packageName")
            intent.putExtra(Intent.EXTRA_RETURN_RESULT, true)
            startActivityForResult(intent, 1)
            //Toast.makeText(this, "Device is not rooted, uninstall manually", Toast.LENGTH_LONG).show()
        }*/

    }

    fun isRootAvailable(): Boolean {
        for (pathDir in System.getenv("PATH").split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
            if (File(pathDir, "su").exists()) {
                return true
            }
        }
        return false
    }
}