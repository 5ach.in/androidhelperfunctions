package in.a5ach.sapnahelperfunctions;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class JavaClass extends AppCompatActivity {

    ArrayList<String> exampleListOfStrings = new ArrayList<>();

    private ImageView exampleImage;

    private EditText exampleUsername;
    private EditText examplePassword;
    private Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);/*
        exampleUsername = findViewById(R.id.username_field);
        examplePassword = findViewById(R.id.password_field);
        submitButton = findViewById(R.id.submit_button);*/

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Username entered is: " + exampleUsername.getText().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
