package `in`.a5ach.sapnahelperfunctions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
class PermissionRequester(private var context: Context, private var permissionString: String, private var permissionRequestCode: Int) {
    fun requestPermission() {
        ActivityCompat.requestPermissions((context as Activity), arrayOf(permissionString), permissionRequestCode)
    }

    fun showSnackBar() {

        /* USE THIS CODE TO CUSTOMISE THE SNACKBAR
        val snackbar = Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Replace with your own action",
                Snackbar.LENGTH_LONG).setAction("Action", null)
        snackbar.setActionTextColor(Color.BLUE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(Color.LTGRAY)
        val textView =
                snackbarView.findViewById(android.support.design.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.BLUE)
        textView.textSize = 28f
        snackbar.show()
        */

        /* EXAMPLE OF STRING MANIPULATION
        val arrayAfterDotSplit: List<String> = permissionString.split(",")
        val sizeOfArrayAfterDotSplit: Int = arrayAfterDotSplit.size
        val permissionNameLastInArray: String = arrayAfterDotSplit[sizeOfArrayAfterDotSplit - 1]
        val convertUnderscoresToWhiteSpace: String = permissionNameLastInArray.replace("_", " ")
        val convertToLowerCase: String = convertUnderscoresToWhiteSpace.toLowerCase()
        val convertFirstCharacterToUpperCase: Char = convertToLowerCase[0].toUpperCase()
        val getSubstringExceptFirstCharacter: String = convertToLowerCase.substring(1)
        val joinFirstUpperCharacterAndLoweredSubstring: String = convertFirstCharacterToUpperCase + getSubstringExceptFirstCharacter
        val finalMessage: String = joinFirstUpperCharacterAndLoweredSubstring + " is disabled" // "Record audio is disabled"

        val oneLiner: String = "${((permissionString.split(".")[permissionString.split(".").size - 1])[0] + ((permissionString.split(".")[permissionString.split(".").size - 1]).toLowerCase().replace("_", " ")).substring(1))} is disabled"
        */

        var snackBar = Snackbar.make((context as Activity).window.decorView.findViewById(android.R.id.content), "\"${((permissionString.split(".")[permissionString.split(".").size - 1])[0] + ((permissionString.split(".")[permissionString.split(".").size - 1]).toLowerCase().replace("_", " ")).substring(1))}\" permission denied", Snackbar.LENGTH_LONG)
        snackBar.setAction("ENABLE") {
            snackBar.dismiss()
            openPermissionSettings()
        }
        snackBar.show()
    }

    fun openPermissionSettings(){
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:${(context as Activity).packageName}"))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }
}