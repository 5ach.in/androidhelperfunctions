package `in`.a5ach.sapnahelperfunctions

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem

class ThemeChange : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }
}
