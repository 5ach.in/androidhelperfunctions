package `in`.a5ach.sapnahelperfunctions

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class BookPagesPagerAdapter(fragmentManager: FragmentManager, private val entryId: ArrayList<Int>) :
        FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return BookPageFragment.newInstance(entryId[position])
    }

    override fun getCount(): Int {
        return entryId.size
    }
}