package `in`.a5ach.sapnahelperfunctions

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button

class TransparentActivity : AppCompatActivity() {
    private var ticketCancel: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transparent_activity)
        ticketCancel = findViewById(R.id.ticketCancel)
        ticketCancel!!.setOnClickListener { finish() }
    }
}
