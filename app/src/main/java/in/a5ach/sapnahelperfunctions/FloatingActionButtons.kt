package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast

class FloatingActionButtons : AppCompatActivity() {

    internal var countDownTimerDraw: CountDownTimer? = null
    internal var countDownTimerCamera: CountDownTimer? = null
    internal var wantInfoDraw: Boolean = false
    internal var wantInfoCamera = true
    internal var infoToast: Toast? = null

    private var addDream: FloatingActionButton? = null
    private var fabDraw:FloatingActionButton? = null
    private var fabCamera:FloatingActionButton? = null

    internal var fabOpen:Animation? = null
    internal var fabQuickOpen:Animation? = null
    internal var fabClose:Animation? = null
    internal var fabQuickClose:Animation? = null
    internal var fabRotateClock:Animation? = null
    internal var fabRotateAntiClock:Animation? = null
    internal var expand:Animation? = null
    internal var contract:Animation? = null

    internal var pageSelected: Int? = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_floating_action_buttons)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName


        fabOpen = AnimationUtils.loadAnimation(this, R.anim.open)
        fabQuickOpen = AnimationUtils.loadAnimation(this, R.anim.quickopen)
        fabClose = AnimationUtils.loadAnimation(this, R.anim.close)
        fabQuickClose = AnimationUtils.loadAnimation(this, R.anim.quickclose)
        fabRotateClock = AnimationUtils.loadAnimation(this, R.anim.rotate_clockwise)
        fabRotateAntiClock = AnimationUtils.loadAnimation(this, R.anim.rotate_anticlockwise)
        expand = AnimationUtils.loadAnimation(this, R.anim.expand)
        contract = AnimationUtils.loadAnimation(this, R.anim.contract)
        addDream = findViewById(R.id.fabAddDream)
        fabDraw = findViewById(R.id.fabDraw)
        fabCamera = findViewById(R.id.fabCamera)
        fabDraw?.setColorFilter(Color.GRAY)
        fabCamera?.setColorFilter(Color.GRAY)


        countDownTimerDraw = object : CountDownTimer(2000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                infoToast = Toast.makeText(applicationContext, "Draw", Toast.LENGTH_SHORT)
                infoToast?.show()
            }
        }

        countDownTimerCamera = object : CountDownTimer(2000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                infoToast = Toast.makeText(applicationContext, "Camera", Toast.LENGTH_SHORT)
                infoToast?.show()
            }
        }

        addDream?.setOnTouchListener(object : View.OnTouchListener {
            var startX: Float = 0.toFloat()
            var startRawX: Float = 0.toFloat()
            var startY: Float = 0.toFloat()
            var startRawY: Float = 0.toFloat()

            override fun onTouch(view: View, event: MotionEvent): Boolean {

                when (event.actionMasked) {
                    MotionEvent.ACTION_DOWN -> {
                        startX = view.x - event.rawX
                        startRawX = event.rawX
                        startY = view.y - event.rawY
                        startRawY = event.rawY

                        addDream?.startAnimation(fabClose)
                        addDream?.visibility = View.GONE
                        //fabDraw.startAnimation(fabOpen);
                        fabDraw?.startAnimation(fabOpen)

                        fabDraw?.visibility = View.VISIBLE
                        fabCamera?.startAnimation(fabOpen)
                        fabCamera?.visibility = View.VISIBLE
                    }
                    MotionEvent.ACTION_MOVE -> {
                        val totalX = event.rawX - startRawX
                        val totalY = (event.rawY - startRawY) * -1

                        if (totalX > -100 && totalX < 200 && totalY > 20 && totalY < 2000) {
                            setDrawFabOn()
                            setCameraFabOff()
                            pageSelected = 1
                        }

                        if (totalX > -2000 && totalX < -40 && totalY > -200 && totalY < 100) {
                            setCameraFabOn()
                            setDrawFabOff()
                            pageSelected = 2
                        } else if (!(totalX > -2000 && totalX < -40 && totalY > -200 && totalY < 100) && !(totalX > -100 && totalX < 200 && totalY > 20 && totalY < 2000)) {
                            setCameraFabOff()
                            setDrawFabOff()
                            pageSelected = 0

                        }
                    }
                    MotionEvent.ACTION_UP -> {

                        if (pageSelected == 1) {
                            setDrawFabOn()
                            setCameraFabOff()
                            openActivity(pageSelected)
                            fabCamera?.startAnimation(fabQuickClose)
                            fabDraw?.startAnimation(fabQuickClose)
                            fabDraw?.visibility = View.GONE
                            fabCamera?.visibility = View.GONE
                        } else if (pageSelected == 2) {
                            setCameraFabOn()
                            setDrawFabOff()
                            openActivity(pageSelected)

                            fabCamera?.startAnimation(fabQuickClose)
                            fabDraw?.startAnimation(fabQuickClose)
                            fabDraw?.visibility = View.GONE
                            fabCamera?.visibility = View.GONE
                        } else {
                            fabDraw?.startAnimation(fabClose)
                            fabDraw?.visibility = View.GONE
                            fabCamera?.startAnimation(fabClose)
                            fabCamera?.visibility = View.GONE
                            addDream?.visibility = View.VISIBLE
                            addDream?.startAnimation(fabOpen)
                        }

                        wantInfoDraw = true
                        wantInfoCamera = true
                    }
                    MotionEvent.ACTION_BUTTON_PRESS -> return false
                    else -> return false
                }/*
                        int rotation = getResources().getConfiguration().orientation;
                        if (rotation == Configuration.ORIENTATION_PORTRAIT) {
                        }
                        else if(rotation == Configuration.ORIENTATION_LANDSCAPE) {
                        }
*//*

                        if(fabDraw.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.ic_menu_camera, getContext().getTheme()).getConstantState())){
                            pageSelected = 1;
                            openActivity(pageSelected);
                            //fabCamera.setVisibility(View.GONE);
                            //fabDraw.setVisibility(View.GONE);
                        }

                        if(fabCamera.getDrawable().getConstantState().equals(getResources().getDrawable(R.mipmap.microphone, getContext().getTheme()).getConstantState())){
                            pageSelected = 2;
                            openActivity(pageSelected);
                         //   fabCamera.setVisibility(View.GONE);
                           // fabDraw.setVisibility(View.GONE);
                        }
                        else{
                            //fabDraw.startAnimation(fabClose);
                            //fabDraw.setVisibility(View.GONE);

                           // fabCamera.startAnimation(fabClose);
                            //fabCamera.setVisibility(View.GONE);
                            //addDream.setVisibility(View.VISIBLE);
                            //addDream.startAnimation(fabOpen);

                            wantInfoDraw = true;
                            wantInfoCamera = true;
                        }

*/
                return true
            }
        })

    }


    private fun setDrawFabOn() {
        fabDraw?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.colorAccent))
        fabDraw?.setColorFilter(Color.WHITE)
        if (wantInfoDraw) {
            countDownTimerDraw?.start()
            wantInfoDraw = false
        }
    }

    private fun setDrawFabOff() {
        fabDraw?.backgroundTintList = ColorStateList.valueOf(Color.WHITE)
        fabDraw?.setColorFilter(Color.GRAY)
        wantInfoDraw = true
        countDownTimerDraw?.cancel()
    }

    private fun setCameraFabOn() {
        fabCamera?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.colorAccent))
        fabCamera?.setColorFilter(Color.WHITE)
        if (wantInfoCamera) {
            countDownTimerCamera?.start()
            wantInfoCamera = false
        }
    }

    private fun setCameraFabOff() {
        fabCamera?.backgroundTintList = ColorStateList.valueOf(Color.WHITE)
        //fabCamera.setImageDrawable(getResources().getDrawable(R.mipmap.microphone, getContext().getTheme()));
        fabCamera?.setColorFilter(Color.GRAY)
        wantInfoCamera = true
        countDownTimerCamera?.cancel()
    }

    private fun openActivity(pageSelected: Int?) {
        if (pageSelected == 1) {
            Toast.makeText(this, "Page selected 1", Toast.LENGTH_SHORT).show()
            if (infoToast != null) {
                infoToast?.cancel()
            }
            setDrawFabOff()
            setCameraFabOff()
        }
        if (pageSelected == 2) {
            if (infoToast != null) {
                infoToast?.cancel()
            }
            setDrawFabOff()
            setCameraFabOff()
            Toast.makeText(this, "Page selected 2", Toast.LENGTH_SHORT).show()
        }
        addDream?.startAnimation(fabQuickOpen)

    }


    public override fun onResume() {
        addDream?.startAnimation(fabQuickOpen)
        super.onResume()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //Toast.makeText(getContext(), "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            //Toast.makeText(getContext(), "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }
}