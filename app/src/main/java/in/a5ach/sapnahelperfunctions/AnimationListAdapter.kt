package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import java.lang.reflect.Field
import android.graphics.drawable.GradientDrawable
import android.util.TypedValue
import android.widget.*


class AnimationListAdapter(val items : Array<Field>, val context: Context, val animationView: View) : RecyclerView.Adapter<ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

/*
        val gd = GradientDrawable()
        gd.setColor(-0xff0100) // Changes this drawbale to use a single color instead of a gradient
        gd.cornerRadius = 5f
        gd.setStroke(1, -0x1000000)
        p0.tvAnimalType.background = gd*/

        val outValue = TypedValue()
        context.theme.resolveAttribute(android.R.attr.selectableItemBackground, outValue, true)
        p0.tvAnimalType.setBackgroundResource(outValue.resourceId)

        p0.tvAnimalType.text = items[p1].name
        p0.tvAnimalType.setOnClickListener {
            animationView.clearAnimation()
            animationView?.startAnimation(AnimationUtils.loadAnimation(context, items[p1].getInt(null)))
            //Toast.makeText(context, "$p1", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.recycler_view_item_layout, p0, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val tvAnimalType = view as Button
}