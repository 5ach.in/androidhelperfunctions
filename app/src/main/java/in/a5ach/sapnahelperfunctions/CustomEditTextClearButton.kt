package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.AppCompatEditText
import android.util.AttributeSet
import android.view.View

class CustomEditTextClearButton : AppCompatEditText {


    private lateinit var mClearButtonImage: Drawable

    constructor(context: Context) : super(context){
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs){
        setUpButton()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle){
    }


    private fun setUpButton(){
        mClearButtonImage = ResourcesCompat.getDrawable(resources, R.drawable.ic_black_half_transparent_24dp, null)!!
        setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, mClearButtonImage, null)

    }

    override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
        if(lengthAfter == 0){
            //mClearButtonImage.setVisible(View.VISIBLE)
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (mClearButtonImage == null) {
            return
        }
        /*// right offset for showing errors in the EditText
        var rightOffset = 0
        if (error != null && error.length > 0) {
            rightOffset = resources.displayMetrics
                    .density.toInt() * 32
        }

        var right = width - paddingRight - rightOffset

        var top = paddingTop
        var bottom = height - paddingBottom
        var ratio = mClearButtonImage.getIntrinsicWidth() as Float / mClearButtonImage.getIntrinsicHeight() as Float
        rightOffset = 0
        if (error != null && error.length > 0) {
            rightOffset = resources.displayMetrics
                    .density.toInt() * 32
        }

        right = width - paddingRight - rightOffset

        top = paddingTop
        bottom = height - paddingBottom
        ratio = mClearButtonImage.getIntrinsicWidth() as Float / mClearButtonImage.getIntrinsicHeight() as Float
        //if images are the correct size.
        //int left = right - mCurrentDrawable.getIntrinsicWidth();
        //scale image depending on height available.
        val left = (right - (bottom - top) * ratio).toInt()
        mClearButtonImage.setBounds(left, top, right, bottom)
*/
        mClearButtonImage.draw(canvas)

    }
}