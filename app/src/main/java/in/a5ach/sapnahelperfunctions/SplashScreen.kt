package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.os.AsyncTask
import android.content.ComponentName
import android.content.pm.PackageManager
import android.os.CountDownTimer
import android.support.design.widget.Snackbar
import android.widget.ProgressBar
import android.widget.TextView

class SplashScreen : AppCompatActivity() {

    private lateinit var timer: CountDownTimer
    var mProgressBar: ProgressBar? = null
    var asdasd: TextView? = null
    internal var runner = AsyncTaskRunner()
    override fun onCreate(savedInstanceState: Bundle?) {
        //setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)



        mProgressBar = findViewById(R.id.progressbar_timerview)
        asdasd = findViewById(R.id.asdasd)
        mProgressBar?.max = 10000




        timer = object : CountDownTimer(10000, 1) { // starts at 3 seconds

            override fun onTick(secondsUntilDone: Long) {
                mProgressBar?.progress = secondsUntilDone.toInt()
                asdasd?.text = "$secondsUntilDone"
                //Toast.makeText(applicationContext, "Opened app time(s)}", Toast.LENGTH_LONG).show()

            }

            override fun onFinish() {
            }
        }.start()



        var isFirstTime = (getSharedPreferences("SplashScreen", Context.MODE_PRIVATE).getInt("IsFirstTime", 0))
        //Toast.makeText(applicationContext, "Opened app ${isFirstTime + 1} time(s)}", Toast.LENGTH_LONG).show()

        getSharedPreferences("SplashScreen", Context.MODE_PRIVATE).edit().putInt("IsFirstTime", isFirstTime + 1).commit()
        if (isFirstTime == 0) {

            var snackbar = Snackbar.make(window.decorView.findViewById(android.R.id.content), "If you want to use app as a developer, click OK", 10000)
            snackbar?.setAction("OK") {
                getSharedPreferences("MainActivity", Context.MODE_PRIVATE).edit().putString("Dev", "DEV").commit()
                snackbar?.dismiss()
            }
            snackbar.addCallback(object : Snackbar.Callback() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)

                    //runner.cancel(true)
                    runner = AsyncTaskRunner()
                    runner.execute(null)
                    val intent = Intent(baseContext, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                }
            })
            snackbar.show()
        }
        else{
            val intent = Intent(baseContext, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }



    }

    inner class AsyncTaskRunner : AsyncTask<Void, String, Boolean>() {
        override fun doInBackground(vararg params: Void): Boolean? {
            //setShortCut(applicationContext, getString(R.string.app_name))
            //createShortCut()
            var iconSet = (getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).getString("Icon", "Default"))
            removeOtherIcons()
            return true
        }
        override fun onPostExecute(result: Boolean) {
            if (result) { }
        }
        override fun onPreExecute() {}
        override fun onProgressUpdate(vararg text: String) {}
    }

    fun removeOtherIcons() {
        var launcherActivityFullName: String
        var launcherActivityName: String
        val info = packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
        if (info.activities != null) {
            launcherActivityFullName = (packageManager.getLaunchIntentForPackage(packageName).addCategory(Intent.CATEGORY_LAUNCHER)).component.className
            for (p in info.activities) {
                launcherActivityName = launcherActivityFullName.substring(packageName.length + 1, launcherActivityFullName.length)
                var activityName = p.name.substring(packageName.length + 1)
                if (activityName.length > launcherActivityName.length) {
                    val activityShortName = activityName.substring(launcherActivityName.length)
                    val activityPrefixName = activityName.substring(0, launcherActivityName.length)
                    if (activityPrefixName.equals(launcherActivityName)) {
                        getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).edit().putString("launcherActivityFullName", launcherActivityFullName).commit()
                        var numberOfIcons = (getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).getInt("NumberOfIcons", 0))
                        getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).edit().putInt("NumberOfIcons", numberOfIcons + 1).commit()
                        getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).edit().putInt("IconImageId${numberOfIcons + 1}", p.icon).commit()
                        getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).edit().putString("IconImageName${numberOfIcons + 1}", activityShortName).commit()
                        packageManager.setComponentEnabledSetting(
                                ComponentName(packageName, "$launcherActivityFullName$activityShortName"),
                                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP)
                    }
                }
            }
        }
    }
}