package `in`.a5ach.sapnahelperfunctions

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import android.widget.ArrayAdapter

class RequestPermissions : AppCompatActivity() {

    private var permissionsList: ListView? = null
    private var permissionRequester: PermissionRequester? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_permessions)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

        permissionsList = findViewById(R.id.permissionsList)
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, resources.getStringArray(R.array.permission_names))
        permissionsList!!.adapter = arrayAdapter
        permissionsList!!.setOnItemClickListener { parent, view, position, id ->
            permissionRequester = PermissionRequester(this, "${parent.getItemAtPosition(position)}", resources.getInteger(resources.getIdentifier("${parent.getItemAtPosition(position)}_REQUEST_CODE", "integer", packageName)))
            permissionRequester!!.requestPermission()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.settings_menu, menu)
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "${((permissions[0].split(".")[permissions[0].split(".").size - 1])[0] + ((permissions[0].split(".")[permissions[0].split(".").size - 1]).toLowerCase().replace("_", " ")).substring(1))} is granted", Toast.LENGTH_LONG).show()
        } else if (grantResults.isNotEmpty()) {
            permissionRequester?.showSnackBar()
        }
        return

        /*when (requestCode) {
            resources.getInteger(R.integer.android_permission_RECORD_AUDIO_REQUEST_CODE) -> {
                if (grantResults.size > 0 && grantResults[0] == MainActivity.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Yaayy able to record audio", Toast.LENGTH_LONG).show()
                } else if (grantResults.isNotEmpty()) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
                        permissionRequester!!.showSnackBar()
                    }
                }
                return
            }
        }*/
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.settings -> {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:$packageName"))
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}