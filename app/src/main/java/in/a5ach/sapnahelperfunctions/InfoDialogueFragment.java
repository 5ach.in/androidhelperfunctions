package in.a5ach.sapnahelperfunctions;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.content.ClipboardManager;
import android.widget.ImageButton;
import android.widget.Toast;


import java.util.ArrayList;

import static android.content.Context.CLIPBOARD_SERVICE;

public class InfoDialogueFragment extends DialogFragment {
    private NetworkChecker networkChecker;

    private Button animationCancel;
    private ImageButton copyGitlabLink;
    private ImageButton openGitlabLink;

    private ArrayList<EditText> listOfEditTexts = new ArrayList<>();
    private View root;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View ticketDialogueFragmentMainView = inflater.inflate(R.layout.custom_info_dialog_fragment, container, false);
        this.getDialog().setCanceledOnTouchOutside(true);
        root = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (root instanceof ViewGroup) {
                    findAllEdittexts((ViewGroup) root);
                }
            }
        });

        networkChecker = new NetworkChecker(getActivity());


        animationCancel = ticketDialogueFragmentMainView.findViewById(R.id.animationCancel);
        copyGitlabLink = ticketDialogueFragmentMainView.findViewById(R.id.copyGitlabLink);
        openGitlabLink = ticketDialogueFragmentMainView.findViewById(R.id.openGitlabLink);

        copyGitlabLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", getActivity().getResources().getString(R.string.gitlabLink));
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getActivity(), "'" + getActivity().getResources().getString(R.string.gitlabLink) + "' copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });
        openGitlabLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (networkChecker.isNetworkAvailable()) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+getActivity().getResources().getString(R.string.gitlabLink)));
                    startActivity(browserIntent);
                }
                else{
                    Toast.makeText(getActivity(), "Network not available", Toast.LENGTH_SHORT).show();
                }

            }
        });


        animationCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                }
        });

        WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();
        wmlp.gravity = Gravity.FILL_HORIZONTAL;
        return ticketDialogueFragmentMainView;
    }

    private void findAllEdittexts(ViewGroup viewGroup) {
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                findAllEdittexts((ViewGroup) view);
            else if (view instanceof EditText) {
                listOfEditTexts.add((EditText) view);
            } else {
                view.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        //((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
                        //rootView.requestFocusFromTouch();
                        for (View editText : listOfEditTexts) {
                            if (editText.isFocused()) {
                                editText.clearFocus();
                            }
                        }
                        return false;
                    }
                });
            }
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                //Toast.makeText(getActivity(), "CHANGES NOT SAVED", Toast.LENGTH_LONG).show();
                dismiss();
            }
        };
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        //startActivity(new Intent(getActivity(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }
}