package `in`.a5ach.sapnahelperfunctions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.AsyncTask
import android.provider.MediaStore
import android.support.design.widget.AppBarLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.animation.*
import android.view.animation.AnimationUtils.loadAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import java.io.FileNotFoundException
import java.io.InputStream


class Animations : AppCompatActivity() {
    var context: Context = this
    var animationView: View? = null
    var animationTextView: TextView? = null
    var animationRecyclerView: RecyclerView? = null
    private var permissionRequester: PermissionRequester? = null
    var SELECT_FILE: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animations)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

        animationView = findViewById(R.id.animationView)
        animationTextView = findViewById(R.id.animationTextView)

        var animationNames = ArrayList<String>()
        val drawablesFields = R.anim::class.java.fields

        for (field in drawablesFields) {
            try {
                animationNames.add(field.name)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        animationRecyclerView = findViewById(R.id.animationRecyclerView)
        animationRecyclerView?.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        var adapter = AnimationListAdapter(R.anim::class.java.fields, this, animationView!!)
        animationRecyclerView?.adapter = adapter

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.image -> {
                permissionRequester = PermissionRequester(context, resources.getStringArray(R.array.permission_names)[25 - 1], resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[25 - 1]}_REQUEST_CODE", "integer", packageName)))
                permissionRequester!!.requestPermission()
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.image_menu, menu)
        return true
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            (resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[25 - 1]}_REQUEST_CODE", "integer", packageName))) -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    intent.type = "image/*"
                    startActivityForResult(intent, SELECT_FILE)
                } else if (grantResults.isNotEmpty()) {
                    permissionRequester?.showSnackBar()
                }
                return
            }
        }
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SELECT_FILE) {
                val runner = AsyncTaskRunner()
                runner.execute(data!!.data)
            }
        }

    private inner class AsyncTaskRunner : AsyncTask<Uri, String, Boolean>() {
        internal var selectedImage: Bitmap? = null
        internal var vi: LayoutInflater? = null
        internal var v: View? = null
        var test_imageview: ImageView? = null
        override fun doInBackground(vararg params: Uri): Boolean? {
            var imageStream: InputStream? = null
            try {
                imageStream = contentResolver.openInputStream(params[0])
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }

            selectedImage = BitmapFactory.decodeStream(imageStream)
            selectedImage = getResizedBitmap(selectedImage!!, 400)// 400 is for example, replace with desired size

            vi = applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            v = vi!!.inflate(R.layout.test_image_row, null)
            test_imageview = v!!.findViewById(R.id.test_imageview)
            test_imageview?.setImageBitmap(selectedImage)

            return true
        }

        override fun onPostExecute(result: Boolean?) {
            animationView?.background = BitmapDrawable(resources, selectedImage)
        }
        override fun onPreExecute() {}

        override fun onProgressUpdate(vararg text: String) {}
    }

    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.width
        var height = image.height
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }

}


