package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.os.Vibrator
import android.os.VibrationEffect
import android.support.annotation.RequiresApi
import android.view.MotionEvent
import android.widget.*
import android.os.CountDownTimer
import android.view.View.OnTouchListener
import android.widget.TextView

class CustomVibration : AppCompatActivity() {

    var customVibrationTapText: TextView? = null
    var customVibrationTapArea: RelativeLayout? = null
    var customVibrationTapMessage: TextView? = null
    var customVibrationPlayRepeatButton: ImageButton? = null
    var customVibrationPlayVisualSeekBar: SeekBar? = null
    var customVibrationPlayVisualStartTime: TextView? = null
    var customVibrationPlayVisualEndTime: TextView? = null

    var isRecordingLoop: Boolean = false
    var customVibrationStopButton: Button? = null
    var isRecordingVibration: Boolean = false
    var listOfLongForVibrationPattern: ArrayList<Long> = ArrayList<Long>()
    var startTime: Long = 0
    var newStartTime: Long = 0
    var playVisuaTotalTime: Long = 0
    var vibrationPatternString: String = ""

    var vibrator: Vibrator? = null
    var countDownTimer: CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_vibration)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

        vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        customVibrationTapText = findViewById(R.id.customVibrationTapText)
        customVibrationTapArea = findViewById(R.id.customVibrationTapArea)
        customVibrationTapMessage = findViewById(R.id.customVibrationTapMessage)
        customVibrationPlayRepeatButton = findViewById(R.id.customVibrationPlayRepeatButton)
        customVibrationPlayVisualSeekBar = findViewById(R.id.customVibrationPlayVisualSeekBar)
        customVibrationPlayVisualSeekBar?.setOnTouchListener { _, _ -> true }

        customVibrationPlayVisualStartTime = findViewById(R.id.customVibrationPlayVisualStartTime)
        customVibrationPlayVisualEndTime = findViewById(R.id.customVibrationPlayVisualEndTime)
        customVibrationStopButton = findViewById(R.id.customVibrationStopButton)

        if((getSharedPreferences("CustomVibration", Context.MODE_PRIVATE).getInt("customVibrationRepeat", 1)) == 0){
            customVibrationPlayRepeatButton?.setColorFilter(resources.getColor(R.color.green))
            isRecordingLoop = true
        }
        vibrationPatternString = getSharedPreferences("CustomVibration", Context.MODE_PRIVATE).getString("customVibrationPattern", "0")
        var result: List<Long> = vibrationPatternString.split(",").map { it.toLong() }
        result.forEach {
            listOfLongForVibrationPattern.add(it)
            playVisuaTotalTime = playVisuaTotalTime + it
        }
        customVibrationPlayVisualSeekBar?.max = (playVisuaTotalTime).toInt()
        customVibrationPlayVisualEndTime?.text = ((playVisuaTotalTime)/1000).toString() + ":" + ((playVisuaTotalTime)%1000).toString()
        customVibrationTapArea?.setOnTouchListener(OnTouchListener { v, event ->
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    if (!isRecordingVibration) {
                        isRecordingVibration = true
                        customVibrationStopButton?.visibility = View.VISIBLE
                        listOfLongForVibrationPattern.clear()
                        customVibrationTapText?.text = ""
                        customVibrationTapText?.text = "0"
                        listOfLongForVibrationPattern.add(0)
                        startTime = System.currentTimeMillis()
                        customVibrationTapMessage?.visibility = View.INVISIBLE
                    } else {
                        listOfLongForVibrationPattern.add(System.currentTimeMillis() - newStartTime)
                        customVibrationTapText?.text = customVibrationTapText?.text.toString() + ", " + (System.currentTimeMillis() - newStartTime).toString()
                        startTime = System.currentTimeMillis()
                    }
                    return@OnTouchListener true
                }
                MotionEvent.ACTION_UP -> {
                    if (isRecordingVibration) {
                        listOfLongForVibrationPattern.add(System.currentTimeMillis() - startTime)
                        customVibrationTapText?.text = customVibrationTapText?.text.toString() + ", " + (System.currentTimeMillis() - startTime).toString()
                        newStartTime = System.currentTimeMillis()
                    }
                }
            }

            v?.onTouchEvent(event) ?: true
        })

        customVibrationStopButton?.setOnClickListener { v ->
            listOfLongForVibrationPattern.add(System.currentTimeMillis() - newStartTime)
            customVibrationTapText?.text = customVibrationTapText?.text.toString() + ", " + (System.currentTimeMillis() - newStartTime).toString()
            isRecordingVibration = false
            v?.visibility = View.GONE
            customVibrationTapMessage?.visibility = View.VISIBLE
            playVisuaTotalTime = 0
            listOfLongForVibrationPattern.forEach {
                playVisuaTotalTime = playVisuaTotalTime + it
            }
            customVibrationPlayVisualSeekBar?.max = (playVisuaTotalTime).toInt()
            customVibrationPlayVisualEndTime?.text = ((playVisuaTotalTime)/1000).toString() + ":" + ((playVisuaTotalTime)%1000).toString()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }


    fun loopPlayback(v: View) {
        if (!isRecordingLoop) {
            isRecordingLoop = true
            customVibrationPlayRepeatButton?.setColorFilter(resources.getColor(R.color.green))
        }
        else{
            isRecordingLoop = false
            customVibrationPlayRepeatButton?.setColorFilter(resources.getColor(R.color.black))
        }
    }
    fun saveVibrationToSharedPref(v: View) {
        vibrationPatternString = ""
        for (i in 0 until listOfLongForVibrationPattern.size) {
            if(i==0){
                vibrationPatternString = listOfLongForVibrationPattern[i].toString()
            }
            else{
                vibrationPatternString = vibrationPatternString.toString() + "," + listOfLongForVibrationPattern[i].toString()
            }
        }
        getSharedPreferences("CustomVibration", Context.MODE_PRIVATE).edit().putString("customVibrationPattern", vibrationPatternString).commit()
        var repeater = -1
        if(isRecordingLoop){
            repeater = 0
        }
        getSharedPreferences("CustomVibration", Context.MODE_PRIVATE).edit().putInt("customVibrationRepeat", repeater).commit()
        Toast.makeText(this, "Vibration saved to SharedPreferences", Toast.LENGTH_SHORT).show()
    }

    //@RequiresApi(api = Build.VERSION_CODES.O)
    fun playVibration(v: View) {
        val button = v as Button
        if(button.text.toString().toLowerCase() == "stop"){
            button.text = "PLAY"
            vibrator?.cancel()
            countDownTimer?.cancel()
            customVibrationPlayVisualSeekBar?.progress = 0
            customVibrationPlayVisualStartTime?.text = 0.toString()
        }
        else{
            var repeater: Int = -1
            if(isRecordingLoop){
                repeater = 0
                button.text = "STOP"
            }
            /*
            val mVibratePattern = longArrayOf(0, 400, 1000, 600, 1000, 800, 1000, 1000)
            val mAmplitudes = intArrayOf(0, 255, 0, 255, 0, 255, 0, 255)
            val effect = VibrationEffect.createWaveform(mVibratePattern, mAmplitudes, -1)
            vibrator.vibrate(effect)*/
            vibrator?.vibrate(listOfLongForVibrationPattern.toLongArray(), repeater)



            countDownTimer = object : CountDownTimer((customVibrationPlayVisualSeekBar?.max)!!.toLong(), 1) {
                override fun onTick(millisUntilFinished: Long) {
                    customVibrationPlayVisualSeekBar?.progress = (customVibrationPlayVisualSeekBar?.max!! - millisUntilFinished).toInt()
                    customVibrationPlayVisualStartTime?.text = ((customVibrationPlayVisualSeekBar?.max!! - millisUntilFinished)/1000).toString() + ":" + ((customVibrationPlayVisualSeekBar?.max!! - millisUntilFinished)%1000).toString()
                }
                override fun onFinish() {
                    customVibrationPlayVisualSeekBar?.progress = 0
                    customVibrationPlayVisualStartTime?.text = 0.toString()
                    if(isRecordingLoop){
                        this.start()
                    }
                }
            }.start()
        }

    }

    override fun onPause() {
        super.onPause()
        vibrator?.cancel()

    }

}
