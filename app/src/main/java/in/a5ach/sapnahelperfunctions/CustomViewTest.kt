package `in`.a5ach.sapnahelperfunctions


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast


class CustomViewTest : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_view_test)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName


        val mkEditText = findViewById(R.id.CustomEditTextClearButton2) as CustomEditTextClearButton2
        mkEditText.showRightIcon()
        mkEditText.setIconClickListener(object : CustomEditTextClearButton2.IconClickListener {
            override fun onClick() {
                Toast.makeText(applicationContext, "${mkEditText.text}", Toast.LENGTH_LONG).show()

                // You can do action here. ex you can start activity here like this
                //startActivityForResult(Intent(applicationContext, MainActivity::class.java), 1)
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }
}



