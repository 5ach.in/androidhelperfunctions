package in.a5ach.sapnahelperfunctions;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class CaptureScreenshotBroadcastReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;

    // Triggered by the Alarm periodically (starts the service to run task)
    @Override
    public void onReceive(Context context, Intent intent) {
        //Toast.makeText(context, "BroadcastReceiver", Toast.LENGTH_SHORT).show();

        Intent i = new Intent(context, CaptureScreenIntentService.class);
        i.putExtra("foo", "bar");
        context.startService(i);
    }
}