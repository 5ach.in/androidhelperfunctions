package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.widget.*
import java.util.ArrayList
import android.support.v4.view.ViewPager.OnPageChangeListener
import android.util.TypedValue
import android.support.v7.widget.Toolbar
import android.support.design.widget.AppBarLayout
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout




class BookPageSlider : AppCompatActivity(), BookPageFragment.BackListener, BookPageFragment.ForwardListener {


    var pagerForwardBar: View? = null
    var pagerBackBar: View? = null


    override fun onForwardAction(view: View) {
        pagerForwardBar = view
        redoText()
        bookPageSliderViewPager?.setSwipePagingEnabled(false)
        pagerForwardBar?.isClickable = false
    }

    override fun onBackAction(view: View) {
        pagerBackBar = view
        undoText()
        bookPageSliderViewPager?.setSwipePagingEnabled(false)
        pagerBackBar?.isClickable = false
    }

    var toolbar: Toolbar? = null
    var appBarLayout: AppBarLayout? = null

    var bookmarkMenu: MenuItem? = null
    var bookmarked: Int = 0

    private lateinit var pagerAdapter: BookPagesPagerAdapter/*
    private var title = ArrayList<String>()
    private var description = ArrayList<String>()
    private var images = ArrayList<Int>()*/
    private var entryIds = ArrayList<Int>()


    //private var bookPageSliderBoxForegroundView: View? = null
    private var bookPageSliderViewPager: BookPageCustomViewPager? = null
    private var bookPageSliderBar: LinearLayout? = null

    private var textHistoryUndo: ImageButton? = null
    private var textHistoryRedo: ImageButton? = null
    private var textBookmarkSeekBar: SeekBar? = null
    private var textHistorySeekBar: SeekBar? = null
    private var textHistorySeekBarValue: TextView? = null
    private var textBookmarkSeekBarValue: TextView? = null
    private var fromHistory: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_page_slider)
        title = javaClass.simpleName

        toolbar = findViewById(R.id.toolbar)

        appBarLayout = findViewById(R.id.appBarLayout)
        appBarLayout?.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow = true
            var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {

                if(verticalOffset < -60){
                    appBarLayout?.setExpanded(false, false)
                }
                else{
                    appBarLayout?.setExpanded(true, false)
                }

                /*
                Toast.makeText(applicationContext, "verticalOffset ${verticalOffset}", Toast.LENGTH_SHORT).show()

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    toolbar?.title = ""
                    isShow = true
                } else if (isShow) {
                    toolbar?.title = "Title "//carefull there should a space between double quote otherwise it wont work
                    isShow = false
                }*/
            }
        })




        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        bookmarked = getSharedPreferences("BookPageSlider", Context.MODE_PRIVATE).getInt("bookmarked", -1)


        //bookPageSliderBoxForegroundView = findViewById(R.id.bookPageSliderBoxForegroundView)
        bookPageSliderViewPager = findViewById(R.id.bookPageSliderViewPager)
        bookPageSliderBar = findViewById(R.id.bookPageSliderBar)

        textHistoryUndo = findViewById(R.id.textHistoryUndo)
        textHistoryRedo = findViewById(R.id.textHistoryRedo)
        textBookmarkSeekBar = findViewById(R.id.textBookmarkSeekBar)
        textHistorySeekBar = findViewById(R.id.textHistorySeekBar)
        textHistorySeekBarValue = findViewById(R.id.textHistorySeekBarValue)
        textBookmarkSeekBarValue = findViewById(R.id.textBookmarkSeekBarValue)

        textBookmarkSeekBar?.setOnTouchListener(View.OnTouchListener { view, motionEvent -> true })

        val loop = getSharedPreferences("BookPageSlider", Context.MODE_PRIVATE).getInt("entriesCount", 420000)
        for (i in 0 until loop) {
            entryIds.add(i)
        }

        pagerAdapter = BookPagesPagerAdapter(supportFragmentManager, entryIds)
        bookPageSliderViewPager?.adapter = pagerAdapter

        bookPageSliderViewPager?.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                if(state == ViewPager.SCROLL_STATE_IDLE){
                    bookPageSliderViewPager?.setSwipePagingEnabled(true)
                    pagerForwardBar?.isClickable = true
                    pagerBackBar?.isClickable = true
                }
            }
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                //Toast.makeText(applicationContext, "position ${bookPageSliderViewPager!!.currentItem} + bookmarked ${bookmarked}", Toast.LENGTH_SHORT).show()
                textHistorySeekBar?.progress = position
                if(bookmarked == bookPageSliderViewPager!!.currentItem){
                    bookmarkMenu?.setIcon(R.drawable.ic_bookmark_black_24dp)
                }
                else{
                    bookmarkMenu?.setIcon(R.drawable.ic_bookmark_border_black_24dp)
                }
            }

        })

        textBookmarkSeekBar?.max = entryIds.size-1
        textHistorySeekBar?.max = entryIds.size-1
        if(bookmarked == -1 || (bookmarked>entryIds.size-1)){
            textBookmarkSeekBar?.visibility = View.GONE
            textHistorySeekBar?.progress = entryIds.size-1
            bookPageSliderViewPager?.setCurrentItem(entryIds.size-1, true)
            textHistorySeekBarValue?.text = "${entryIds.size} out of ${entryIds.size}"
        }
        else{
            textBookmarkSeekBar?.progress = bookmarked
            textHistorySeekBar?.progress = bookmarked
            bookPageSliderViewPager?.setCurrentItem(bookmarked, true)
            textHistorySeekBarValue?.text = "${bookmarked+1} out of ${entryIds.size}"
            textBookmarkSeekBarValue?.text = "Bookmarked: ${bookmarked+1}"
        }


        if (bookPageSliderViewPager!!.currentItem > 0) {
            textHistoryUndo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorAccent))
        }
        if (bookPageSliderViewPager!!.currentItem < entryIds.size-1) {
            textHistoryRedo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorAccent))
        }



        textHistorySeekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar) {
                //bookPageSliderViewPager?.visibility = View.INVISIBLE
                //bookPageSliderBoxForegroundView?.visibility = View.VISIBLE
                //bookPageSliderBoxForegroundView?.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.green))

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                //bookPageSliderViewPager?.visibility = View.VISIBLE
                //bookPageSliderBoxForegroundView?.visibility = View.INVISIBLE
            }

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                   // Toast.makeText(applicationContext, "progress ${progress}", Toast.LENGTH_SHORT).show()
                    bookPageSliderViewPager?.setCurrentItem(progress, true)
                }

                if (progress == 0) {
                    textHistoryUndo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.lightgrey))
                } else {
                    textHistoryUndo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorAccent))
                }

                if (progress == entryIds.size - 1) {
                    textHistoryRedo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.lightgrey))
                } else {
                    textHistoryRedo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorAccent))
                }

                textHistorySeekBarValue?.text = "${progress + 1} out of ${entryIds.size}"
            }
        })

        textHistoryUndo?.setOnTouchListener(object : View.OnTouchListener {
            var speed: Long = 500
            var rect: Rect? = null
            var handler = Handler()
            val runnableCode = object : Runnable {
                override fun run() {
                    speed = ((speed % 2) + 50)
                    undoText()
                    handler?.postDelayed(this, speed)
                }
            }

            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        fromHistory = true
                        rect = Rect(v!!.left, v.top, v.right, v.bottom)
                        if (textHistorySeekBar!!.progress > 0) {
                            val outValue = TypedValue()
                            theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true)
                            textHistoryUndo?.setBackgroundResource(outValue.resourceId)
                        }
                        if (!(handler.hasMessages(210))) {
                            handler.postDelayed(runnableCode, 500)
                            handler.sendEmptyMessage(210)
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        if (rect!!.contains(v!!.left + event.x.toInt(), v.top + event.y.toInt())) {

                            undoText()

                        }
                        textHistoryUndo?.clearFocus()
                        textHistoryUndo?.setBackgroundColor(Color.TRANSPARENT)
                        handler.removeCallbacksAndMessages(null)
                    }
                    MotionEvent.ACTION_MOVE -> {
                        if ((!rect!!.contains(v!!.left + event.x.toInt(), v.top + event.y.toInt())) || textHistorySeekBar!!.progress <= 0) {
                            textHistoryUndo?.setBackgroundColor(Color.TRANSPARENT)
                            handler.removeCallbacksAndMessages(null)
                        } else {
                            val outValue = TypedValue()
                            theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true)
                            textHistoryUndo?.setBackgroundResource(outValue.resourceId)
                            if (!(handler.hasMessages(210))) {
                                handler.postDelayed(runnableCode, 500)
                                handler.sendEmptyMessage(210)
                            }
                        }
                    }
                }
                return v?.onTouchEvent(event) ?: false
            }
        })


        textHistoryRedo?.setOnTouchListener(object : View.OnTouchListener {
            var speed: Long = 500
            var rect: Rect? = null
            var handler = Handler()
            val runnableCode = object : Runnable {
                override fun run() {
                    speed = ((speed % 2) + 50)
                    redoText()
                    handler?.postDelayed(this, speed)
                }
            }

            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        fromHistory = true
                        rect = Rect(v!!.left, v.top, v.right, v.bottom)
                        if (textHistorySeekBar!!.progress < textHistorySeekBar!!.max) {
                            val outValue = TypedValue()
                            theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true)
                            textHistoryRedo?.setBackgroundResource(outValue.resourceId)
                        }
                        if (!(handler.hasMessages(210))) {
                            handler.postDelayed(runnableCode, 500)
                            handler.sendEmptyMessage(210)
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        if (rect!!.contains(v!!.left + event.x.toInt(), v.top + event.y.toInt())) {

                            redoText()

                        }
                        textHistoryRedo?.clearFocus()
                        textHistoryRedo?.setBackgroundColor(Color.TRANSPARENT)
                        handler.removeCallbacksAndMessages(null)
                    }
                    MotionEvent.ACTION_MOVE -> {
                        if ((!rect!!.contains(v!!.left + event.x.toInt(), v.top + event.y.toInt())) || textHistorySeekBar!!.progress >= textHistorySeekBar!!.max) {
                            textHistoryRedo?.setBackgroundColor(Color.TRANSPARENT)
                            handler.removeCallbacksAndMessages(null)
                        } else {
                            val outValue = TypedValue()
                            theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true)
                            textHistoryRedo?.setBackgroundResource(outValue.resourceId)
                            if (!(handler.hasMessages(210))) {
                                handler.postDelayed(runnableCode, 500)
                                handler.sendEmptyMessage(210)
                            }
                        }
                    }
                }
                return v?.onTouchEvent(event) ?: false
            }
        })
    }

    fun undoText() {
        fromHistory = true
        if (textHistorySeekBar!!.progress > 0) {
            textHistorySeekBar?.progress = textHistorySeekBar!!.progress - 1
            bookPageSliderViewPager?.setCurrentItem(bookPageSliderViewPager!!.currentItem-1, true)
        }

    }

    fun redoText() {
        fromHistory = true
        if (textHistorySeekBar!!.progress < entryIds.size) {
            textHistorySeekBar?.progress = textHistorySeekBar!!.progress + 1
            bookPageSliderViewPager?.setCurrentItem(bookPageSliderViewPager!!.currentItem+1, true)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.bookpage -> {
                showHistorySliderBar()
                return true
            }
            R.id.bookmark -> {
                if(bookmarked == bookPageSliderViewPager!!.currentItem){
                    bookmarkMenu?.setIcon(R.drawable.ic_bookmark_border_black_24dp)
                    getSharedPreferences("BookPageSlider", Context.MODE_PRIVATE).edit().putInt("bookmarked", -1).commit()
                    textBookmarkSeekBar?.visibility = View.INVISIBLE
                    bookmarked = -1
                    textBookmarkSeekBarValue?.text = ""

                }
                else{
                    bookmarkMenu?.setIcon(R.drawable.ic_bookmark_black_24dp)
                    bookmarked = bookPageSliderViewPager!!.currentItem
                    getSharedPreferences("BookPageSlider", Context.MODE_PRIVATE).edit().putInt("bookmarked", bookPageSliderViewPager!!.currentItem).commit()
                    textBookmarkSeekBar?.visibility = View.VISIBLE
                    textBookmarkSeekBar?.progress = bookPageSliderViewPager!!.currentItem
                    bookmarked = bookPageSliderViewPager!!.currentItem
                    textBookmarkSeekBarValue?.text = "Bookmarked: ${bookmarked+1}"
                }
                bookmarked
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        appBarLayout?.setExpanded(true, true)
        menuInflater.inflate(R.menu.bookpage_bookmark_menu, menu)
        bookmarkMenu = menu.findItem(R.id.bookmark)
        if (bookmarked != -1 && (bookmarked<=entryIds.size-1)) {
            bookmarkMenu?.setIcon(R.drawable.ic_bookmark_black_24dp)
        }
        return true
    }

    private fun showHistorySliderBar() {
        if(bookPageSliderBar!!.visibility == View.VISIBLE){
            val params = bookPageSliderBar?.layoutParams as RelativeLayout.LayoutParams
            //bookPageSliderBar!!.animate().scaleX(0.3f).scaleY(0.3f).setDuration(600).start()
            bookPageSliderBar?.clearAnimation()
            var animation = AnimationUtils.loadAnimation(this, R.anim.shrink_fade)
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    bookPageSliderBar?.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.transparent))
                }
                override fun onAnimationEnd(animation: Animation) {
                    bookPageSliderBar?.visibility = View.GONE
                }
                override fun onAnimationRepeat(animation: Animation) {
                }
            })
            bookPageSliderBar?.startAnimation(animation)

/*
            for (i in 0 until 150) {
                params.height = 150-1
                bookPageSliderBar?.layoutParams = params
            }*/
            //bookPageSliderBar?.visibility = View.GONE
        }
        else{
            bookPageSliderBar?.clearAnimation()
            var animation = AnimationUtils.loadAnimation(this, R.anim.unshrink_unfade)
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    bookPageSliderBar?.visibility = View.VISIBLE
                }
                override fun onAnimationEnd(animation: Animation) {
                    bookPageSliderBar?.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.white))
                }
                override fun onAnimationRepeat(animation: Animation) {
                }
            })
            bookPageSliderBar?.startAnimation(animation)
        }
    }

}
