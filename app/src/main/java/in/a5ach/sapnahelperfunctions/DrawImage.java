package in.a5ach.sapnahelperfunctions;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DrawImage extends AppCompatActivity {

    private static final int REQUEST_WRITE_STORAGE = 112;




    private Paint paint;

    private DrawImageCanvasView drawImageCanvasView;

    private Spinner colSpinner;

    public static ImageButton undoButton, redoButton;

    public static SeekBar drawingBar, drawingBrushBar;
    public static TextView seekBarImageText, seekBarImageBrushText;
    boolean isDrawingBarTouched = false;

    public static int brushDefaultSize;
    public static int brushCurrentSize;
    public static int brushDefaultColorR;
    public static int brushCurrentColorR;
    public static int brushDefaultColorG;
    public static int brushCurrentColorG;
    public static int brushDefaultColorB;
    public static int brushCurrentColorB;

    public static RelativeLayout seekBarImageLayout, seekBarImageBrushLayout;
    public static Boolean isDrawHistory = false;
    public static Boolean isDrawBrush = false;


    public static MenuItem brush;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_image);






        setTitle(getClass().getSimpleName());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_delete_forever_black_24dp);



        boolean hasPermission = (ContextCompat.checkSelfPermission(DrawImage.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }

        brushDefaultSize = 10;
        brushCurrentSize = brushDefaultSize;


        brushDefaultColorR = 255;
        brushCurrentColorR = brushDefaultColorR;
        brushDefaultColorG = 0;
        brushCurrentColorG = brushDefaultColorG;
        brushDefaultColorB = 0;
        brushCurrentColorB = brushDefaultColorB;






        DrawImageCanvasView.paths.clear();
        DrawImageCanvasView.colors.clear();
        DrawImageCanvasView.brushes.clear();


        seekBarImageBrushLayout = (RelativeLayout) findViewById(R.id.seekBarImageBrushLayout);
        seekBarImageBrushText = (TextView) findViewById(R.id.seekBarImageBrushText);
        drawingBrushBar = (SeekBar) findViewById(R.id.drawingBrushBar);
        drawingBrushBar.setMax(250);
        drawingBrushBar.setProgress(brushCurrentSize);
        seekBarImageBrushText.setText(String.valueOf(brushCurrentSize));

        //canvasView.setBrush(drawingBrushBar.getProgress());


        drawingBrushBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isDrawingBarTouched = false;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isDrawingBarTouched = true;
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser == true) {
                    brushCurrentSize = progress;
                    drawImageCanvasView.setBrush(progress);
                    seekBarImageBrushText.setText(String.valueOf(progress));
                    ShapeDrawable badgeSize = new ShapeDrawable(new OvalShape());
                    badgeSize.setIntrinsicWidth(progress);
                    badgeSize.setIntrinsicHeight(progress);
                    badgeSize.getPaint().setColor(Color.rgb(brushCurrentColorR, brushCurrentColorG, brushCurrentColorB));
                    brush.setIcon(badgeSize);

                }
            }
        });


        seekBarImageLayout = (RelativeLayout) findViewById(R.id.seekBarImageLayout);
        seekBarImageText = (TextView) findViewById(R.id.seekBarImageText);
        undoButton = (ImageButton) findViewById(R.id.undoButton);
        redoButton = (ImageButton) findViewById(R.id.redoButton);
        undoButton.setColorFilter(Color.GRAY);
        redoButton.setColorFilter(Color.GRAY);

        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawImageCanvasView.onClickUndo();
            }
        });

        undoButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                deleteDialog();
                return false;
            }
        });


        DrawImageCanvasView.currentPaths.clear();
        DrawImageCanvasView.previousPaths.clear();
        DrawImageCanvasView.pathsHash.clear();
        DrawImageCanvasView.pathsListOLists.clear();
        DrawImageCanvasView.pathsListOLists.add(new ArrayList<Path>());

        /*CanvasView.allPaths.add(new Path());
        CanvasView.allColors.add(new Integer(0));
        CanvasView.allBrushes.add(new Float(0));*/
        drawingBar = (SeekBar) findViewById(R.id.drawingBar);

        drawingBar.setMax(DrawImageCanvasView.previousPaths.size());
        drawingBar.incrementProgressBy(1);

        drawingBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isDrawingBarTouched = false;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isDrawingBarTouched = true;
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser == true) {
                    drawImageCanvasView.onChangeBar(progress);
                }
            }
        });


        drawImageCanvasView = (DrawImageCanvasView) findViewById(R.id.canvas);
        drawImageCanvasView.setBrush(brushCurrentSize);
        drawImageCanvasView.setColour(brushCurrentColorR, brushCurrentColorG, brushCurrentColorB);







        //canvasView.setBackground(getResources().getDrawable(R.drawable.ic_menu_camera));
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        int rotation = getWindowManager().getDefaultDisplay().getRotation();

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels; // PREVENT USER FROM CHANGING SCREEN ORIENTATION, WHICH MESSES UP CANVAS
        int height = dm.heightPixels;
        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width ||
                (rotation == Surface.ROTATION_90
                        || rotation == Surface.ROTATION_270) && width > height) {
            switch (rotation) {
                case Surface.ROTATION_0:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
                case Surface.ROTATION_90:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
                case Surface.ROTATION_180:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                    break;
                case Surface.ROTATION_270:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                    break;
                default:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
            }
        }


        colSpinner = (Spinner) findViewById(R.id.colSpinner);
        List<String> colors = new ArrayList<String>();
        String[] colorLabels = new String[]{"Red", "Black", "Green", "Blue", "Pink"};
        for (int x = 0; x < colorLabels.length; x++) {
            colors.add(colorLabels[x]);
        }
        ArrayAdapter<String> colArray = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, colors);
        colArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Spinner sItems2 = (Spinner) findViewById(R.id.spinProdCat);
        colSpinner.setAdapter(colArray);
        colSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            ShapeDrawable badge = new ShapeDrawable(new OvalShape());

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        brushCurrentColorR = 255;
                        brushCurrentColorG = 0;
                        brushCurrentColorB = 0;
                        break;
                    case 1:
                        brushCurrentColorR = 0;
                        brushCurrentColorG = 0;
                        brushCurrentColorB = 0;
                        break;
                    case 2:
                        brushCurrentColorR = 26;
                        brushCurrentColorG = 155;
                        brushCurrentColorB = 0;
                        break;
                    case 3:
                        brushCurrentColorR = 0;
                        brushCurrentColorG = 9;
                        brushCurrentColorB = 255;
                        break;
                    case 4:
                        brushCurrentColorR = 255;
                        brushCurrentColorG = 0;
                        brushCurrentColorB = 230;
                        break;
                }
                drawImageCanvasView.setColour(brushCurrentColorR, brushCurrentColorG, brushCurrentColorB);
                badge.setIntrinsicWidth(brushCurrentSize);
                badge.setIntrinsicHeight(brushCurrentSize);
                badge.getPaint().setColor(Color.rgb(brushCurrentColorR, brushCurrentColorG, brushCurrentColorB));
                brush.setIcon(badge);
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                // sometimes you need nothing here
            }


        });


    }

    public void clearCanvas(View v) {
        deleteDialog();
        Toast.makeText(DrawImage.this, "Clear", Toast.LENGTH_SHORT).show();
    }

    public void undoCanvas(View v) {
        drawImageCanvasView.onClickUndo();
        //Toast.makeText(DrawImage.this, "Undho", Toast.LENGTH_SHORT).show();


    }

    public void redoCanvas(View v) {
        drawImageCanvasView.onClickRedo();
        //canvasView.setColour(255, 155, 245);
        //final int MY_COLOR = Color.rgb(255, 102, 153);
        //int bob = colSpinner.getSelectedItemPosition();


        //Toast.makeText(DrawImage2.this, "Redho" + bob, Toast.LENGTH_SHORT).show();
    }


    private void deleteDialog() {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
        deleteDialog.setTitle("Are you sure you want to");
        deleteDialog.setMessage("clear canvas and delete drawing history?");
        deleteDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                drawImageCanvasView.clearCanvas();
                dialog.dismiss();
            }
        });
        deleteDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        deleteDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        //final MenuItem menuItem = menu.add(Menu.NONE, 1000, Menu.NONE, R.string.action_save);
        //MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_IF_ROOM);
        getMenuInflater().inflate(R.menu.drawimage, menu);
        brush = menu.findItem(R.id.action_brush);
        ShapeDrawable optionsIconSet = new ShapeDrawable(new OvalShape());
        optionsIconSet.setIntrinsicWidth(brushCurrentSize);
        optionsIconSet.setIntrinsicHeight(brushCurrentSize);

        optionsIconSet.getPaint().setColor(Color.rgb(brushCurrentColorR, brushCurrentColorG, brushCurrentColorB));
        brush.setIcon(optionsIconSet);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_save) {
            Bitmap bitmap = Bitmap.createBitmap(drawImageCanvasView.getHeight(), drawImageCanvasView.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawImageCanvasView.setBackgroundColor(Color.WHITE);
            //canvasView.setBackground(getResources().getDrawable(R.drawable.ic_menu_camera));

            drawImageCanvasView.draw(canvas);
            //bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            store(bitmap, "bob");
            return true;
        }

        if (id == R.id.action_history) {
            if (isDrawHistory == false) {
                seekBarImageLayout.clearAnimation();
                seekBarImageLayout.setVisibility(View.VISIBLE);
                seekBarImageLayout.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slideupfrombottom));
                seekBarImageLayout.bringToFront();
                isDrawHistory = true;
            } else {
                seekBarImageLayout.setVisibility(View.GONE);
                isDrawHistory = false;
            }

            return true;
        }

        if (id == R.id.action_brush) {
            if (isDrawBrush == false) {


                /*if(isDrawHistory==true){
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);

                    seekBarImageLayout.bringToFront();

                }*/


                seekBarImageBrushLayout.clearAnimation();
                seekBarImageBrushLayout.setVisibility(View.VISIBLE);
                seekBarImageBrushLayout.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slidedownfromtop));
                seekBarImageBrushLayout.bringToFront();
                isDrawBrush = true;
            } else {
                seekBarImageBrushLayout.setVisibility(View.GONE);
                isDrawBrush = false;
            }

            return true;
        }

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home/back button
            case android.R.id.home:
                new android.support.v7.app.AlertDialog.Builder(this)
                        .setTitle("Discard changes?")
                        .setMessage("")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        }).setIcon(android.R.drawable.ic_dialog_alert).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void store(Bitmap bm, String fileName) {


//        String dirPath = getExternalCacheDir().getAbsolutePath() + "/images";
//        File dir = new File(dirPath);
//        if (!dir.exists()) {
//            dir.mkdir();
//        }
//        File file = new File(dirPath, fileName);
//        try {
//            FileOutputStream fos = new FileOutputStream(file);
//            bm.compress(Bitmap.CompressFormat.PNG, 100, fos);
//            fos.flush();
//            fos.close();
//            Toast.makeText(DrawImage.this, "Saved", Toast.LENGTH_SHORT).show();
//        } catch (Exception e) {
//            Toast.makeText(DrawImage.this, "Error", Toast.LENGTH_SHORT).show();
//
//        }


        File save_path = null;
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
        {
            try
            {
                File sdCard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                File dir = new File(sdCard.getAbsolutePath() + "/Signs");
                dir.mkdirs();
                File file = new File(dir, "DreamImage_ZZ"+new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime())+ ".png");
                save_path =   file;
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100,baos);
                FileOutputStream f = null;
                f = new FileOutputStream(file);
                MediaScannerConnection.scanFile(this, new String[]{file.getAbsolutePath()}, null, null);
                if (f != null)
                {
                    f.write(baos.toByteArray());
                    f.flush();
                    f.close();
                }
                //Toast.makeText(DrawImage.this, "Saved to folder: " + dir.toString(), Toast.LENGTH_LONG).show();

                Intent intent = new Intent();
                intent.putExtra("editTextValue", "value_here");
                intent.putExtra("imageUri", dir.toString());
                setResult(RESULT_OK, intent);
                finish();



            }
            catch (Exception e)
            {
                Toast.makeText(DrawImage.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
