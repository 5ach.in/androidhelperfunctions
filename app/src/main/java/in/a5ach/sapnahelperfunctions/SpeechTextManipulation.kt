package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Rect
import android.os.Build
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.*
import java.util.*
import android.util.TypedValue
import android.view.*
import android.widget.Toast


class SpeechTextManipulation : AppCompatActivity(), TextToSpeech.OnInitListener, ActionMode.Callback {


    private var continuousSpeechToTextCursorInfo: TextView? = null
    private var continuousSpeechToTextEditText: MultiAutoCompleteTextView? = null
    private var permissionRequester: PermissionRequester? = null
    private var textToSpeech: TextToSpeech? = null

    private var textHistoryUndo: ImageButton? = null
    private var textHistoryRedo: ImageButton? = null
    private var textHistorySeekBar: SeekBar? = null
    private var textHistorySeekBarValue: TextView? = null

    private var descriptionHistory = ArrayList<String>()
    private var descriptionCursorHistory = ArrayList<Int>()

    private var fromHistory: Boolean = false
    private var fromRecording: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_speech_text_manipulation)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

        textToSpeech = TextToSpeech(this, this)
/*



        var text: CharSequence =  intent.getCharSequenceExtra(Intent.EXTRA_PROCESS_TEXT)

        val readonly = intent.getBooleanExtra(Intent.EXTRA_PROCESS_TEXT_READONLY, false)

        if(readonly){
            Toast.makeText(this, "$text is granted", Toast.LENGTH_LONG).show()

        }
*/

        continuousSpeechToTextCursorInfo = findViewById(R.id.continuousSpeechToTextCursorInfo)
        continuousSpeechToTextEditText = findViewById(R.id.continuousSpeechToTextEditText)
        continuousSpeechToTextCursorInfo?.setText("Cursor position: ${continuousSpeechToTextEditText?.selectionEnd} out of ${continuousSpeechToTextEditText?.length()}")
        continuousSpeechToTextEditText?.setText(getSharedPreferences("SpeechTextManipulation", Context.MODE_PRIVATE).getString("continuousSpeechToTextEditText", ""))

        continuousSpeechToTextEditText?.setCustomSelectionActionModeCallback(this)

        //continuousSpeechToTextEditText?.startActionMode(ActionBarCallBack())


        textHistoryUndo = findViewById(R.id.textHistoryUndo)
        textHistoryRedo = findViewById(R.id.textHistoryRedo)
        textHistorySeekBar = findViewById(R.id.textHistorySeekBar)
        textHistorySeekBarValue = findViewById(R.id.textHistorySeekBarValue)


        setEditTextAutoComplete()


/*

        textHistoryUndo?.setOnClickListener{
            undoText()
        }
        textHistoryRedo?.setOnClickListener{
            redoText()
        }

*/



        textHistoryUndo?.setOnTouchListener(object : View.OnTouchListener {
            var speed: Long = 500
            var rect: Rect? = null
            var handler = Handler()
            val runnableCode = object : Runnable {
                override fun run() {
                    speed = ((speed % 2) + 50)
                    undoText()
                    handler?.postDelayed(this, speed)
                }
            }

            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        fromHistory = true
                        rect = Rect(v!!.left, v.top, v.right, v.bottom)
                        if (textHistorySeekBar!!.progress > 0) {
                            val outValue = TypedValue()
                            theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true)
                            textHistoryUndo?.setBackgroundResource(outValue.resourceId)
                        }
                        if (!(handler.hasMessages(210))) {
                            handler.postDelayed(runnableCode, 500)
                            handler.sendEmptyMessage(210)
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        if (rect!!.contains(v!!.left + event.x.toInt(), v.top + event.y.toInt())) {

                            undoText()

                        }
                        textHistoryUndo?.clearFocus()
                        textHistoryUndo?.setBackgroundColor(Color.TRANSPARENT)
                        handler.removeCallbacksAndMessages(null)
                    }
                    MotionEvent.ACTION_MOVE -> {
                        if ((!rect!!.contains(v!!.left + event.x.toInt(), v.top + event.y.toInt())) || textHistorySeekBar!!.progress <= 0) {
                            textHistoryUndo?.setBackgroundColor(Color.TRANSPARENT)
                            handler.removeCallbacksAndMessages(null)
                        } else {
                            val outValue = TypedValue()
                            theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true)
                            textHistoryUndo?.setBackgroundResource(outValue.resourceId)
                            if (!(handler.hasMessages(210))) {
                                handler.postDelayed(runnableCode, 500)
                                handler.sendEmptyMessage(210)
                            }
                        }
                    }
                }
                return v?.onTouchEvent(event) ?: false
            }
        })


        textHistoryRedo?.setOnTouchListener(object : View.OnTouchListener {
            var speed: Long = 500
            var rect: Rect? = null
            var handler = Handler()
            val runnableCode = object : Runnable {
                override fun run() {
                    speed = ((speed % 2) + 50)
                    redoText()
                    handler?.postDelayed(this, speed)
                }
            }

            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        fromHistory = true
                        rect = Rect(v!!.left, v.top, v.right, v.bottom)
                        if (textHistorySeekBar!!.progress < textHistorySeekBar!!.max) {
                            val outValue = TypedValue()
                            theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true)
                            textHistoryRedo?.setBackgroundResource(outValue.resourceId)
                        }
                        if (!(handler.hasMessages(210))) {
                            handler.postDelayed(runnableCode, 500)
                            handler.sendEmptyMessage(210)
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        if (rect!!.contains(v!!.left + event.x.toInt(), v.top + event.y.toInt())) {

                            redoText()

                        }
                        textHistoryRedo?.clearFocus()
                        textHistoryRedo?.setBackgroundColor(Color.TRANSPARENT)
                        handler.removeCallbacksAndMessages(null)
                    }
                    MotionEvent.ACTION_MOVE -> {
                        if ((!rect!!.contains(v!!.left + event.x.toInt(), v.top + event.y.toInt())) || textHistorySeekBar!!.progress >= textHistorySeekBar!!.max) {
                            textHistoryRedo?.setBackgroundColor(Color.TRANSPARENT)
                            handler.removeCallbacksAndMessages(null)
                        } else {
                            val outValue = TypedValue()
                            theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true)
                            textHistoryRedo?.setBackgroundResource(outValue.resourceId)
                            if (!(handler.hasMessages(210))) {
                                handler.postDelayed(runnableCode, 500)
                                handler.sendEmptyMessage(210)
                            }
                        }
                    }
                }
                return v?.onTouchEvent(event) ?: false
            }
        })


/*
        continuousSpeechToTextEditText?.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    //MotionEvent.ACTION_UP -> continuousSpeechToTextCursorInfo?.text = "Cursor position: ${continuousSpeechToTextEditText?.selectionEnd} out of ${continuousSpeechToTextEditText?.length()}"
                }
                return v?.onTouchEvent(event) ?: true
            }
        })*/
        continuousSpeechToTextEditText?.addTextChangedListener(object : TextWatcher {
            var textBefore: String? = null
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                textBefore = continuousSpeechToTextEditText?.text.toString().trim().toLowerCase()
                if (!fromHistory) {
                    if (!(textBefore.equals(descriptionHistory[descriptionHistory.size - 1].trim().toLowerCase()))) {
                        descriptionHistory.add(continuousSpeechToTextEditText?.text.toString())
                        descriptionCursorHistory.add(continuousSpeechToTextEditText!!.selectionStart)
                        textHistorySeekBarValue?.text = "${descriptionHistory.size} out of ${descriptionHistory.size}"
                        textHistorySeekBar?.max = descriptionHistory.size - 1
                        textHistorySeekBar?.progress = descriptionHistory.size - 1
                    }
                }
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (!fromHistory) {
                    if (!(textBefore.equals(continuousSpeechToTextEditText?.text.toString().trim().toLowerCase()))) {
                        descriptionHistory.add(continuousSpeechToTextEditText?.text.toString())
                        descriptionCursorHistory.add(continuousSpeechToTextEditText!!.selectionStart)
                        textHistorySeekBarValue?.text = "${descriptionHistory.size} out of ${descriptionHistory.size}"
                        textHistorySeekBar?.max = descriptionHistory.size - 1
                        textHistorySeekBar?.progress = descriptionHistory.size - 1
                    }

                }
                continuousSpeechToTextCursorInfo?.setText("Cursor position: ${continuousSpeechToTextEditText!!.selectionEnd} out of ${continuousSpeechToTextEditText?.length()}")
                fromHistory = false
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        descriptionHistory.add(getSharedPreferences("SpeechTextManipulation", Context.MODE_PRIVATE).getString("continuousSpeechToTextEditText", ""))
        descriptionCursorHistory.add((getSharedPreferences("SpeechTextManipulation", Context.MODE_PRIVATE).getString("continuousSpeechToTextEditText", "")).length)
        continuousSpeechToTextEditText?.setText(descriptionHistory[0])
        continuousSpeechToTextEditText?.setSelection(descriptionCursorHistory[0])
        textHistorySeekBar?.max = 0

/*

        val permissions = arrayOf(Manifest.permission.RECORD_AUDIO)
        val REQUEST_RECORD_AUDIO_PERMISSION = 200

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION)

*/

        textHistorySeekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    fromHistory = true
                    continuousSpeechToTextEditText?.setText(descriptionHistory[progress])
                    continuousSpeechToTextEditText?.setSelection(descriptionCursorHistory[textHistorySeekBar!!.progress])
                }

                if (progress == 0) {
                    textHistoryUndo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.lightgrey))
                } else {
                    textHistoryUndo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorAccent))
                }

                if (progress == descriptionHistory.size - 1) {
                    textHistoryRedo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.lightgrey))
                } else {
                    textHistoryRedo?.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorAccent))
                }

                textHistorySeekBarValue?.text = "${progress + 1} out of ${descriptionHistory.size}"
            }
        })

/*
        textHistoryUndo?.setOnClickListener {
            continuousSpeechToTextEditText?.setText("")
            textHistorySeekBar?.progress = textHistorySeekBar!!.progress-1
        }
        textHistoryRedo?.setOnClickListener {
            continuousSpeechToTextEditText?.setText("")
            textHistorySeekBar?.progress = textHistorySeekBar!!.progress+1
        }*/
    }


    fun undoText() {
        fromHistory = true
        if (textHistorySeekBar!!.progress > 0) {
            textHistorySeekBar?.progress = textHistorySeekBar!!.progress - 1
            continuousSpeechToTextEditText?.setText(descriptionHistory[textHistorySeekBar!!.progress])
            continuousSpeechToTextEditText?.setSelection(descriptionCursorHistory[textHistorySeekBar!!.progress])
        }

    }

    fun redoText() {
        fromHistory = true
        if (textHistorySeekBar!!.progress < descriptionHistory.size) {
            textHistorySeekBar?.progress = textHistorySeekBar!!.progress + 1
            continuousSpeechToTextEditText?.setText(descriptionHistory[textHistorySeekBar!!.progress])
            continuousSpeechToTextEditText?.setSelection(descriptionCursorHistory[textHistorySeekBar!!.progress])
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            (resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[9 - 1]}_REQUEST_CODE", "integer", packageName))) -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "${((permissions[0].split(".")[permissions[0].split(".").size - 1])[0] + ((permissions[0].split(".")[permissions[0].split(".").size - 1]).toLowerCase().replace("_", " ")).substring(1))} is granted", Toast.LENGTH_LONG).show()
                } else if (grantResults.isNotEmpty()) {
                    permissionRequester?.showSnackBar()
                }
                return
            }
        }
    }

    fun playTextToSpeech(v: View) {
        speakOut()
    }

    private fun speakOut() {
        val text = continuousSpeechToTextEditText!!.text.toString()






        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textToSpeech!!.speak(text, TextToSpeech.QUEUE_FLUSH, null, "")
        }
        else{
            Toast.makeText(this, "Android device is not able to use this function", Toast.LENGTH_LONG).show()
        }
    }

    public override fun onDestroy() {
        // Shutdown TTS
        if (textToSpeech != null) {
            textToSpeech!!.stop()
            textToSpeech!!.shutdown()
        }
        super.onDestroy()
    }


    override fun onInit(p0: Int) {
        if (p0 == TextToSpeech.SUCCESS) {
            // set US English as language for tts
            val result = textToSpeech!!.setLanguage(Locale.US)

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "The Language specified is not supported!")
            } else {
                //textToSpeechButton!!.isEnabled = true
            }

        } else {
            Log.e("TTS", "Initilization Failed!")
        }
    }


    fun saveTextToSharedPref() {
        getSharedPreferences("SpeechTextManipulation", Context.MODE_PRIVATE).edit().putString("continuousSpeechToTextEditText", continuousSpeechToTextEditText?.getText().toString().trim()).commit()
        Toast.makeText(this, "Text saved to SharedPreferences", Toast.LENGTH_SHORT).show()
    }

    fun recordAudio(v: View) {
        var intArray = ArrayList<Int>()
        intArray.add(9)
        intArray.forEach {
            permissionRequester = PermissionRequester(this, resources.getStringArray(R.array.permission_names)[it - 1], resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[it - 1]}_REQUEST_CODE", "integer", packageName)))
            permissionRequester!!.requestPermission()
        }

        var match = "NOT YET COMPLETE"

        var cursorPosition = continuousSpeechToTextEditText!!.selectionStart
        if (!(continuousSpeechToTextEditText!!.isFocused)) {
            cursorPosition = continuousSpeechToTextEditText!!.text.length
        }

        val orignalText = continuousSpeechToTextEditText?.text
        val orignalTextFirstHalf = orignalText?.substring(0, continuousSpeechToTextEditText!!.selectionStart)
        val orignalTextSecondHalf = orignalText?.substring(continuousSpeechToTextEditText!!.selectionEnd, orignalText.length)

        var charAtStart: Char? = null
        if (orignalTextFirstHalf!!.isNotEmpty()) {
            charAtStart = orignalText[cursorPosition - 1]
        }
        var charAtEnd: Char? = null
        if (orignalTextSecondHalf!!.isNotEmpty()) {
            charAtEnd = orignalText[cursorPosition]
        }

        if (!(charAtStart == ' ' || charAtStart == null || charAtStart.toString() == "\n")) {
            match = " $match"
        }
        if (!(charAtEnd == ' ' || charAtEnd == null || charAtEnd.toString() == "\n")) {
            match = "$match "
        }
        continuousSpeechToTextEditText?.setText(orignalTextFirstHalf + match + orignalTextSecondHalf)
        continuousSpeechToTextEditText?.setSelection(orignalTextFirstHalf.length + match.length)


    }

    fun setEditTextAutoComplete() {
        var LABELS = ArrayList<String>()
        var autoCompleteWordsCount = getSharedPreferences("AutoCompleteWords", Context.MODE_PRIVATE).getInt("autoCompleteWordsCount", 0)
        for (i in 0 until autoCompleteWordsCount) {
            LABELS.add(getSharedPreferences("AutoCompleteWords", Context.MODE_PRIVATE).getString("autoCompleteWords${i + 1}", ""))
        }
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, LABELS)
        continuousSpeechToTextEditText?.setTokenizer(this.SpaceTokenizer())
        continuousSpeechToTextEditText?.setAdapter(adapter)
    }

    inner class SpaceTokenizer : MultiAutoCompleteTextView.Tokenizer {
        override fun findTokenStart(text: CharSequence, cursor: Int): Int {
            var i = cursor
            //while (i > 0 && String.valueOf(text.charAt(i-1)) != "@") {
            while (i > 0 && text[i - 1] != ' ') {
                i--
            }
            return i
        }

        override fun findTokenEnd(text: CharSequence, cursor: Int): Int {
            return text.length
        }

        override fun terminateToken(text: CharSequence): CharSequence {
            return text.toString() + " "
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.add -> {
                val dialogFragment = AddAutoCompleteWordDialogueFragment()
                dialogFragment.show(supportFragmentManager, "addAutoCompleteWord")
                return true
            }
            R.id.save -> {
                saveTextToSharedPref()
                return true
            }
            R.id.tag -> {
                saveTextToSharedPref()
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.add_save_menu, menu)
        return true
    }


    override fun onActionModeStarted(mode: ActionMode) {
        //mode.menu.clear()
        super.onActionModeStarted(mode)
        val menus = mode.menu
        mode.menuInflater.inflate(R.menu.text_tag_menu, menus)
    }

    override fun onActionItemClicked(p0: ActionMode?, item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.tag -> {
                val selectedText = continuousSpeechToTextEditText?.text.toString().substring(continuousSpeechToTextEditText!!.selectionStart, continuousSpeechToTextEditText!!.selectionEnd)
                Toast.makeText(baseContext, "Added: $selectedText", Toast.LENGTH_SHORT).show()
                var autoCompleteWordsCount = getSharedPreferences("AutoCompleteWords", Context.MODE_PRIVATE).getInt("autoCompleteWordsCount", 0)
                getSharedPreferences("AutoCompleteWords", Context.MODE_PRIVATE).edit().putInt("autoCompleteWordsCount", autoCompleteWordsCount + 1).commit()
                getSharedPreferences("AutoCompleteWords", Context.MODE_PRIVATE).edit().putString("autoCompleteWords${autoCompleteWordsCount + 1}", (selectedText.trim())).commit()
                setEditTextAutoComplete()
            }
        }
        p0?.finish()
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateActionMode(p0: ActionMode?, p1: Menu?): Boolean {
        return true
    }

    override fun onPrepareActionMode(p0: ActionMode?, p1: Menu?): Boolean {
        return false
    }

    override fun onDestroyActionMode(p0: ActionMode?) {
    }
}
