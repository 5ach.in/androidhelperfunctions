package `in`.a5ach.sapnahelperfunctions
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.net.Uri

class Uninstall: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(Intent.ACTION_DELETE)
        intent.data = Uri.parse("package:${applicationContext.packageName}")
        startActivity(intent)
    }
}