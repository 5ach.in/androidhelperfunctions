package `in`.a5ach.sapnahelperfunctions

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast

import java.util.ArrayList
import java.util.Calendar

class KotlinClass : AppCompatActivity() {

    internal var exampleListOfStrings = ArrayList<String>()

    private val exampleImage: ImageView? = null

    private var exampleUsername: EditText? = null
    private var examplePassword: EditText? = null
    private var submitButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)/*
        setContentView(R.layout.activity_java_class)
        exampleUsername = findViewById(R.id.username_field)
        examplePassword = findViewById(R.id.password_field)
        submitButton = findViewById(R.id.submit_button)*/

        submitButton!!.setOnClickListener { Toast.makeText(applicationContext, "Username entered is: " + exampleUsername!!.text.toString(), Toast.LENGTH_LONG).show() }

    }

}
