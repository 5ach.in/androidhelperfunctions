package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import android.util.DisplayMetrics
import android.view.ViewConfiguration
import android.support.v4.os.HandlerCompat.postDelayed





class BookPageFragment : Fragment() {
    private var entryId: Int = 0
    private var title: String? = null
    private var description: String? = null
    private var image: Int = 0

    internal lateinit var backListenerCallback: BackListener
    internal lateinit var forwardListenerCallback: ForwardListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        backListenerCallback = (context as BackListener?)!!
        forwardListenerCallback = (context as ForwardListener?)!!

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.entryId = arguments!!.getInt("entryId", 0)
        title = this.activity!!.getSharedPreferences("BookPageSlider", Context.MODE_PRIVATE).getString("entryTitle" + entryId.toString(), "Dummy title " + (entryId + 1).toString())
        description = this.activity!!.getSharedPreferences("BookPageSlider", Context.MODE_PRIVATE).getString("entryDescription" + entryId.toString(), "Lorem ipsum dolor sit amet, no libris molestie philosophia quo. Vix cu persecuti dissentiet, nam fierent rationibus ea, clita repudiare laboramus vix at. Mel an iracundia definiebas, ex liber impedit repudiandae vix. Sea mentitum persecuti ea, tation aliquam vel id. Sit te numquam ocurreret intellegat, eam id illum ridens. Consul convenire eu sea, erant propriae an nam, soleat minimum voluptaria no mea.\n" + "\n" + "Ad eos quando prodesset, sea autem noluisse in. His fugit saepe tamquam ut. Nulla appetere consequuntur mel ex, ad sit mutat intellegat conclusionemque. Id sit principes incorrupte necessitatibus.\n" + "\n" + "An quem justo saperet vim, ius et erroribus similique, quod melius consequuntur ut cum. Has verterem mediocrem iudicabit no, an etiam tollit vim. Ei eum aperiri sapientem, ea qui liber principes. Et per aeterno epicurei percipit, falli verear vituperata eam et. Vix te prompta pericula deterruisset. Id sit posse ubique primis, vix no eius facer liber, duis pertinacia voluptaria usu ea.\n" + "\n" + "Corpora quaerendum ut eum, eu esse autem volumus sed, no pro primis noluisse. Te minimum ullamcorper eam, populo theophrastus nam no. Habeo appareat nam at. Dictas vivendum has an, nonumes recusabo quo eu. Dicit nostro maiestatis cu quo, cum facilisi eleifend intellegam te. Cum eripuit imperdiet no, ne nobis detraxit sit.\n" + "\n" + "Nam ridens sadipscing ea, eu vim nisl affert. At meis ullum velit has, an eos minim adolescens, ea tation honestatis mel. Exerci fierent antiopam eos at, tibique omittam definitiones eos id. Hinc verterem eleifend ne vel. Quo liber omittantur ad. Iriure ")
        image = arguments!!.getInt("image", 0)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.book_page_fragment, container, false)
        val book_page_title = view.findViewById<TextView>(R.id.book_page_title)
        book_page_title.text = title

        val book_page_description = view.findViewById<TextView>(R.id.book_page_description)
        book_page_description.text = description


        val book_page_image = view.findViewById<ImageView>(R.id.book_page_image)
        book_page_image.setImageResource(image)
        view.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in_slow))

/*
        val leftPageTurn = view.findViewById<View>(R.id.leftPageTurn)
        backListenerCallback.onBackAction(leftPageTurn)

        val rightPageTurn = view.findViewById<View>(R.id.rightPageTurn)
        forwardListenerCallback.onForwardAction(rightPageTurn)*/


        var xValue: Float = 0.toFloat()
        var yValue: Float = 0.toFloat()
        val leftPersentage: Int
        val rightPersentage: Int
        val height: Int
        val width: Int
        val displaymetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displaymetrics)
        height = displaymetrics.heightPixels
        width = displaymetrics.widthPixels

        leftPersentage = (width * 20 / 100)
        rightPersentage = (width * 80 / 100)

        view?.setOnTouchListener(object : View.OnTouchListener {
            var numberOfTaps = 0
            var lastTapTimeMs: Long = 0
            var touchDownMs: Long = 0

            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_UP -> {

                        if (System.currentTimeMillis() - touchDownMs > ViewConfiguration.getTapTimeout()) {
                            numberOfTaps = 0
                            lastTapTimeMs = 0
                            return false
                        }
                        if (numberOfTaps > 0 && System.currentTimeMillis() - lastTapTimeMs < ViewConfiguration.getDoubleTapTimeout()) {
                            numberOfTaps += 1
                        } else {
                            numberOfTaps = 1
                        }
                        lastTapTimeMs = System.currentTimeMillis()

                        if (numberOfTaps == 2) {
                            var distance = (yValue - event.y)
                            //var distance = Math.sqrt(((yValue - event.y)*(yValue - event.y)).toDouble())
                            // var distance = Math.sqrt((Math.sqrt((yValue*yValue).toDouble()) - Math.sqrt((event.y*event.y).toDouble()))*(Math.sqrt((yValue*yValue).toDouble()) - Math.sqrt((event.y*event.y).toDouble())))
                            if (xValue <= leftPersentage) {
                                backListenerCallback.onBackAction(view)
                                //Toast.makeText(context, "pressed 20%, y: ${distance}}", Toast.LENGTH_LONG).show()
                                return false
                            }
                            if (xValue >= rightPersentage) {
                                forwardListenerCallback.onForwardAction(view)
                                //Toast.makeText(context, "pressed 20%, y: ${distance}}", Toast.LENGTH_LONG).show()
                                return false
                            }
                        }
                    }
                    MotionEvent.ACTION_DOWN -> {
                        xValue = event.x
                        yValue = event.y
                        touchDownMs = System.currentTimeMillis()
                    }
                }
                return v?.onTouchEvent(event) ?: true
            }
        })

        return view
    }

    interface BackListener {
        fun onBackAction(view: View)
    }

    interface ForwardListener {
        fun onForwardAction(view: View)
    }

    companion object {
        fun newInstance(entryId: Int): BookPageFragment {
            val fragment = BookPageFragment()
            val args = Bundle()
            args.putInt("entryId", entryId)
            fragment.arguments = args
            return fragment
        }
    }
}