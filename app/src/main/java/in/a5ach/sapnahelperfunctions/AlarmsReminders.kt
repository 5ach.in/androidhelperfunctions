package `in`.a5ach.sapnahelperfunctions

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import java.util.*

class AlarmsReminders : AppCompatActivity() {
    var messageBackgroundService: EditText? = null
    var timeBackgroundService: EditText? = null
    var startBackgroundService: Button? = null
    var stopBackgroundService: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarms_reminders)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

        messageBackgroundService = findViewById(R.id.messageBackgroundService)
        timeBackgroundService = findViewById(R.id.timeBackgroundService)
        messageBackgroundService?.setText((getSharedPreferences("AlarmsReminders", Context.MODE_PRIVATE).getString("Message", "")))

        messageBackgroundService?.addTextChangedListener(object : TextWatcher {
            var textBefore: String? = null
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                getSharedPreferences("AlarmsReminders", Context.MODE_PRIVATE).edit().putString("Message", messageBackgroundService!!.text.toString()).commit()
            }
        })

        startBackgroundService = findViewById(R.id.startBackgroundService)
        stopBackgroundService = findViewById(R.id.stopBackgroundService)


    }

    fun startBackgroundService(v: View) {
        val calendar = Calendar.getInstance()
        /*Intent myIntent = new Intent(getApplicationContext(), CaptureScreenService.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),  0, myIntent, 0);
        AlarmManager alarmManager = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 1); // first time
        long frequency= 1 * 1000; // in ms
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), frequency, pendingIntent);

        Intent intent = new Intent(this, CaptureScreenService.class);
        PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);
        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000, pintent);
*/

        scheduleAlarm()
        val cal = Calendar.getInstance()
        cal.add(Calendar.SECOND, 1)

        val intent = Intent(this, CaptureScreenService::class.java)
        val pintent = PendingIntent.getService(this, 0, intent, 0)
        val alarm = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        //for 30 mint 60*60*1000
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.timeInMillis,1000, pintent)
        startService(Intent(baseContext, CaptureScreenService::class.java))
    }

    fun stopBackgroundService(v: View) {
        stopService(Intent(baseContext, CaptureScreenService::class.java))
        stopService(Intent(baseContext, CaptureScreenIntentService::class.java))
    }

    fun scheduleAlarm() {
        // Construct an intent that will execute the AlarmReceiver
        val intent = Intent(applicationContext, CaptureScreenshotBroadcastReceiver::class.java)
        // Create a PendingIntent to be triggered when the alarm goes off
        val pIntent = PendingIntent.getBroadcast(this, CaptureScreenshotBroadcastReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT)
        // Setup periodic alarm every every half hour from this point onwards
        val alarm = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        // First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
        // Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY
        //alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, AlarmManager.INTERVAL_HALF_HOUR, pIntent);
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000, pIntent)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }
}