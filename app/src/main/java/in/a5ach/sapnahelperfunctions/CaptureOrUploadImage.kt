package `in`.a5ach.sapnahelperfunctions

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import java.io.FileNotFoundException
import java.io.InputStream

class CaptureOrUploadImage : AppCompatActivity() {
    var context: Context = this

    var gridMenu: MenuItem? = null
    var layoutGridOrList: Int = 0
    var REQUEST_CAMERA: Int = 1
    var SELECT_FILE: Int = 0
    var ivImage: ImageView? = null
    var parentLinearLayout: LinearLayout? = null
    private var permissionRequester: PermissionRequester? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_capture_or_upload_image)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

        layoutGridOrList = (getSharedPreferences("CaptureOrUploadImage", Context.MODE_PRIVATE).getInt("layoutGridOrList", 0))
        parentLinearLayout = findViewById(R.id.parentLinearLayout)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.add -> {
                SelectImage()
                return true
            }
            R.id.grid -> {
                if (layoutGridOrList == 0) {
                    layoutGridOrList = 1
                    gridMenu?.setIcon(R.drawable.ic_reorder_black_24dp)
                } else if (layoutGridOrList == 1) {
                    layoutGridOrList = 0
                    gridMenu?.setIcon(R.drawable.ic_grid_on_black_24dp)
                }
                getSharedPreferences("CaptureOrUploadImage", Context.MODE_PRIVATE).edit().putInt("layoutGridOrList", layoutGridOrList).commit()
                return true
            }
            R.id.save -> {
                Toast.makeText(this, "Images saved", Toast.LENGTH_SHORT).show()
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.add_save_grid_menu, menu)
        gridMenu = menu.findItem(R.id.grid)
        if (layoutGridOrList == 0) {
            gridMenu?.setIcon(R.drawable.ic_grid_on_black_24dp)
        } else if (layoutGridOrList == 1) {
            gridMenu?.setIcon(R.drawable.ic_reorder_black_24dp)
        }
        return true
    }

    private fun SelectImage() {
        val items = arrayOf<CharSequence>("Gallery", "Camera")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Image")
        builder.setItems(items, object : DialogInterface.OnClickListener {

            override fun onClick(dialogInterface: DialogInterface, i: Int) {
                if (items[i] == "Camera") {
                    permissionRequester = PermissionRequester(context, resources.getStringArray(R.array.permission_names)[3 - 1], resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[3 - 1]}_REQUEST_CODE", "integer", packageName)))
                    permissionRequester!!.requestPermission()
                } else if (items[i] == "Gallery") {
                    permissionRequester = PermissionRequester(context, resources.getStringArray(R.array.permission_names)[25 - 1], resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[25 - 1]}_REQUEST_CODE", "integer", packageName)))
                    permissionRequester!!.requestPermission()
                } else if (items[i] == "Cancel") {
                    dialogInterface.dismiss()
                }
            }
        })
        builder.show()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            (resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[3 - 1]}_REQUEST_CODE", "integer", packageName))) -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(intent, REQUEST_CAMERA)
                } else if (grantResults.isNotEmpty()) {
                    permissionRequester?.showSnackBar()
                }
                return
            }
            (resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[25 - 1]}_REQUEST_CODE", "integer", packageName))) -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    intent.type = "image/*"
                    startActivityForResult(intent, SELECT_FILE)
                } else if (grantResults.isNotEmpty()) {
                    permissionRequester?.showSnackBar()
                }
                return
            }
        }
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                val bundle = data!!.extras
                val bmp = bundle!!.get("data") as Bitmap
                ivImage?.setImageBitmap(bmp)
            } else if (requestCode == SELECT_FILE) {
                val runner = AsyncTaskRunner()
                runner.execute(data!!.data)
            }
        }
    }

    private inner class AsyncTaskRunner : AsyncTask<Uri, String, Boolean>() {
        internal var selectedImage: Bitmap? = null
        internal var vi: LayoutInflater? = null
        internal var v: View? = null
        var test_imageview: ImageView? = null
        override fun doInBackground(vararg params: Uri): Boolean? {
            var imageStream: InputStream? = null
            try {
                imageStream = contentResolver.openInputStream(params[0])
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }

            selectedImage = BitmapFactory.decodeStream(imageStream)
            selectedImage = getResizedBitmap(selectedImage!!, 400)// 400 is for example, replace with desired size

            vi = applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            v = vi!!.inflate(R.layout.test_image_row, null)
            test_imageview = v!!.findViewById(R.id.test_imageview)
            test_imageview?.setImageBitmap(selectedImage)

            return true
        }

        override fun onPostExecute(result: Boolean?) {
            val fadeIn = AlphaAnimation(0f, 1f)
            fadeIn.interpolator = DecelerateInterpolator() //add this
            fadeIn.duration = 750
            parentLinearLayout?.addView(v)


            val zoomEffect = ScaleAnimation(1.2f, 1f, 1.2f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
            zoomEffect.duration = 100     // animation duration in milliseconds
            zoomEffect.fillAfter = true    // If fillAfter is true, the transformation that this animation performed will persist when it is finished.
            val animation = AnimationSet(false) //change to false
            animation.addAnimation(fadeIn)
            animation.addAnimation(zoomEffect)
            v!!.animation = fadeIn
            test_imageview?.animation = zoomEffect
        }

        override fun onPreExecute() {}

        override fun onProgressUpdate(vararg text: String) {}
    }

    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.width
        var height = image.height
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }

}
