package in.a5ach.sapnahelperfunctions;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.Type;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by Username on 24/01/2018.
 */

public class BlurBackgroundClasses {

    private Context context;

    public BlurBackgroundClasses(Context context){
        this.context=context;
    }

    public void unBlurBackground(ImageView imageViewBlur, FrameLayout linearLayoutNormal){
        imageViewBlur.setVisibility(View.GONE);
        linearLayoutNormal.setVisibility(View.VISIBLE);
    }

    public void blurBackground(ImageView imageViewBlur, FrameLayout linearLayoutNormal){
        linearLayoutNormal.setVisibility(View.INVISIBLE);
        imageViewBlur.setVisibility(View.VISIBLE);
        imageViewBlur.setBackground(createImageToBlur(getBitmapFromView(linearLayoutNormal)));
    }

    public Bitmap getBitmapFromView(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        view.draw(c);
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int maxWidth = 500;
        int maxHeight = 500;
        if (width > height) {
            int ratio = width / maxWidth;
            width = maxWidth;
            height = height / ratio;
        } else if (height > width) {
            int ratio = height / maxHeight;
            height = maxHeight;
            width = width / ratio;
        } else {
            height = maxHeight;
            width = maxWidth;
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        return bitmap;
    }

    public Drawable createImageToBlur(Bitmap bitmap){
        float BLUR_RADIUS = 1;
        RenderScript rs = RenderScript.create(context);
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Type bitmapType = new Type.Builder(rs, Element.RGBA_8888(rs)).setX(width).setY(height).setMipmaps(false).create();
        Allocation allocation = Allocation.createTyped(rs, bitmapType);
        ScriptIntrinsicBlur blurScript = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        blurScript.setRadius(BLUR_RADIUS);
        allocation.copyFrom(bitmap);
        blurScript.setInput(allocation);
        blurScript.forEach(allocation);
        int repeat = 10;
        for (int i=0; i<repeat; i++) {
            blurScript.forEach(allocation);
        }
        allocation.copyTo(bitmap);
        allocation.destroy();
        blurScript.destroy();
        Drawable drawableNew = new BitmapDrawable(context.getResources(), bitmap);
        return drawableNew;
    }
}
