package in.a5ach.sapnahelperfunctions;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import java.util.ArrayList;
import java.util.HashMap;

public class DrawImageCanvasView extends View implements OnTouchListener {
    private Canvas mCanvas;
    private Path mPath;
    private Paint mPaint;

    public static ArrayList<Path> paths = new ArrayList<Path>();
    public static ArrayList<Integer> colors = new ArrayList<Integer>();
    public static ArrayList<Float> brushes = new ArrayList<Float>();
    public static ArrayList<Path> undonePaths = new ArrayList<Path>();
    public static ArrayList<Integer> undoneColors = new ArrayList<Integer>();
    public static ArrayList<Float> undoneBrushes = new ArrayList<Float>();



    public static ArrayList<Path> allPaths = new ArrayList<Path>();
    public static ArrayList<Integer> allColors = new ArrayList<Integer>();
    public static ArrayList<Float> allBrushes = new ArrayList<Float>();


    public static HashMap<Path, Integer> colorsHash = new HashMap<Path, Integer>();
    public static HashMap<Path, Float> brushesHash = new HashMap<Path, Float>();




    Context context;
    private Bitmap mBitmap;
    private int paintColor = 0xFF660000;
    private int backgroundColor = 0xFF660000;




    private int r;
    private int g;
    private int b;
    private float s;
    private int r2;
    private int g2;
    private int b2;

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 5;

    public void setColour(int red, int green, int blue){
        r = red;
        g = green;
        b = blue;
        paintColor = Color.rgb(r, g, b);
        mPaint.setColor(paintColor);
        //mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    }

    public void setBrush(int size){
        s = (float) size;
        mPaint.setStrokeWidth(s);
    }

    public void setBackground(int red, int green, int blue){
        r2 = red;
        g2 = green;
        b2 = blue;
        backgroundColor = Color.rgb(r, g, b);
        mPaint.setStyle(Paint.Style.FILL);
    }


    private void init(){
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setColor(paintColor);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(DrawImage.brushDefaultSize);
        //mPaint.setStrokeWidth(100);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    public DrawImageCanvasView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.setOnTouchListener(this);
        init();
    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }




    public static ArrayList<ArrayList<Path>> pathsListOLists = new ArrayList<ArrayList<Path>>();

    public static HashMap<Integer, ArrayList<Path>> pathsHash = new HashMap<Integer, ArrayList<Path>>();

    public static ArrayList<Path> previousPaths = new ArrayList<Path>();
    public static ArrayList<Path> currentPaths = new ArrayList<Path>();

    public static int pathTimeCount = 0;



    @Override
    protected void onDraw(Canvas canvas) {
        for (Path p : currentPaths) {
            mPaint.setColor(colorsHash.get(p));
            mPaint.setStrokeWidth(brushesHash.get(p));
            canvas.drawPath(p, mPaint);
        }
        mPaint.setColor(paintColor);
        mPaint.setStrokeWidth(s);
        canvas.drawPath(mPath, mPaint);
    }

    private void touch_start(float x, float y) {
        undonePaths.clear();
        undoneColors.clear();
        undoneBrushes.clear();
        mPath.reset();
        mPath.moveTo(x, y);


        if(
        DrawImage.drawingBar.getProgress()!=DrawImage.drawingBar.getMax()){

            ArrayList<Path> listOfPaths = new ArrayList<Path>();
            listOfPaths.addAll(currentPaths);
            //pathsHash.put(pathTimeCount, listOfPaths);
            //Toast.makeText(getContext(), "currentPaths size "+String.valueOf(currentPaths.size()), Toast.LENGTH_SHORT).show();
            pathsListOLists.add(listOfPaths);

            //ADDS PATH UESR WANTS TO START FROM TO THE STACK/AR
        }

        colorsHash.put(mPath, mPaint.getColor());
        brushesHash.put(mPath, mPaint.getStrokeWidth());
        currentPaths.add(mPath);

        mX = x;
        mY = y;
    }
    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;
            mY = y;
        }
    }

    private void touch_up() {
        mPath.lineTo(mX, mY);
        mCanvas.drawPath(mPath, mPaint);
        paths.add(mPath);
        colors.add(mPaint.getColor());
        brushes.add(mPaint.getStrokeWidth());

        if (pathTimeCount > 0) {
            String asd = "";
            for(int x = 0; x < pathsListOLists.size(); x++){
                ArrayList<Path> newPathsstr = pathsListOLists.get(x);
                asd = asd + ", "+String.valueOf(newPathsstr.size());
            }
            //Toast.makeText(getContext(), "new get size "+asd, Toast.LENGTH_SHORT).show();
        }
        DrawImage.drawingBar.setMax(pathsListOLists.size());
        DrawImage.drawingBar.setProgress(DrawImage.drawingBar.getMax());

        pathTimeCount=pathTimeCount+1;
        mPath = new Path();
    }

    public void onChangeBar (int position) {
        if (pathsListOLists.size()>0)
        {
            ArrayList<Path> copyOfPaths = new ArrayList<Path>();
            copyOfPaths.addAll(pathsListOLists.get(position));

            currentPaths = copyOfPaths;
            //currentPaths = pathsHash.get(position+1);

            //Toast.makeText(getContext(), "hash size "+String.valueOf(currentPaths.size()), Toast.LENGTH_SHORT).show();
            //undonePaths.add(previousPaths.remove(previousPaths.size()-1));
            DrawImage.seekBarImageText.setText(String.valueOf(position) + " out of " + String.valueOf(pathsListOLists.size()-1));
            invalidate();
        }
        else
        {
        }
    }



    public void onClickUndo () {
        if (DrawImage.drawingBar.getProgress()>0)
        {
            ArrayList<Path> copyOfPaths = new ArrayList<Path>();
            copyOfPaths.addAll(pathsListOLists.get(DrawImage.drawingBar.getProgress()-1));
            currentPaths = copyOfPaths;
            DrawImage.drawingBar.setProgress(DrawImage.drawingBar.getProgress()-1);
            DrawImage.seekBarImageText.setText(String.valueOf(DrawImage.drawingBar.getProgress()) + " out of " + String.valueOf(pathsListOLists.size()-1));
            invalidate();
        }
        else
        {
        }
    }

    public void onClickRedo (){
        if (DrawImage.drawingBar.getProgress()<DrawImage.drawingBar.getMax())
        {
            ArrayList<Path> copyOfPaths = new ArrayList<Path>();
            copyOfPaths.addAll(pathsListOLists.get(DrawImage.drawingBar.getProgress()+1));
            currentPaths = copyOfPaths;
            DrawImage.drawingBar.setProgress(DrawImage.drawingBar.getProgress()+1);
            DrawImage.seekBarImageText.setText(String.valueOf(DrawImage.drawingBar.getProgress()) + " out of " + String.valueOf(pathsListOLists.size()-1));
            invalidate();
        }
        else
        {
        }
    }

    public void clearCanvas(){
        paths.clear();
        colors.clear();
        brushes.clear();

        undonePaths.clear();
        undoneColors.clear();
        undoneBrushes.clear();

        colorsHash.clear();
        brushesHash.clear();

        currentPaths.clear();
        previousPaths.clear();
        pathsHash.clear();
        pathsListOLists.clear();
        pathsListOLists.add(new ArrayList<Path>());
        DrawImage.drawingBar.setMax(0);
        DrawImage.seekBarImageText.setText(String.valueOf("0 out of 0"));


        mPath.reset();
        invalidate();
        init();
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();

                ArrayList<Path> listOfPaths = new ArrayList<Path>();
                listOfPaths.addAll(currentPaths);
                //pathsHash.put(pathTimeCount, listOfPaths);
                //Toast.makeText(getContext(), "currentPaths size "+String.valueOf(currentPaths.size()), Toast.LENGTH_SHORT).show();
                pathsListOLists.add(listOfPaths);

                DrawImage.seekBarImageText.setText(String.valueOf(pathsListOLists.size()-1) + " out of " + String.valueOf(pathsListOLists.size()-1));

                invalidate();
                break;
        }
        return true;
    }


}
