package in.a5ach.sapnahelperfunctions;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptureScreenIntentService extends IntentService {
    public CaptureScreenIntentService() {
        super("CaptureScreenIntentService");
    }


    @Override
    public void onDestroy() {
        Toast.makeText(getApplicationContext(), "Service Destroy", Toast.LENGTH_SHORT).show();
        super.onDestroy();/*
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, YourService.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 600000, pi); // Millisec * Second * Minute*/

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Handler mHandler = new Handler(getMainLooper());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                String message = (getSharedPreferences("AlarmsReminders", Context.MODE_PRIVATE).getString("Message", "Service executed"));
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });

       /* try {
            Process sh = Runtime.getRuntime().exec("su", null,null);
            OutputStream os = sh.getOutputStream();
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
            String formatDate = df.format(new Date(System.currentTimeMillis()));

            os.write(("/system/bin/screencap -p " + "/sdcard/img"+formatDate+".png").getBytes("ASCII"));
            os.flush();
            os.close();
            sh.waitFor();

            *//*Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.book_page_slider_bookmark_thumb);

            TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();

            Frame imageFrame = new Frame.Builder()
                    .setBitmap(bm)                 // your image bitmap
                    .build();

            SparseArray<TextBlock> textBlocks = textRecognizer.detect(imageFrame);
            for (int i = 0; i < textBlocks.size(); i++) {
                final TextBlock textBlock = textBlocks.get(textBlocks.keyAt(i));
                Handler mHandler = new Handler(getMainLooper());
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), textBlock.getValue(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
*//*


        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Log.i("MyTestService", "Service running");*/
    }
}