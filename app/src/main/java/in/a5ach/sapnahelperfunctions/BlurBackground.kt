package `in`.a5ach.sapnahelperfunctions

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.SeekBar
import android.widget.TextView
import android.content.Context
import android.content.Intent
import android.widget.FrameLayout
import android.widget.ImageView


class BlurBackground : AppCompatActivity() {
    var context: Context = this
    internal var addEditLinearLayout: FrameLayout? = null
    internal var blurBackgroundClasses: BlurBackgroundClasses? = null
    internal var addEditBlurImage: ImageView? = null
    internal var animationView1: View? = null
    internal var animationView2: View? = null
    internal var animationView3: View? = null
    internal var animationView4: View? = null
    internal var animationView5: View? = null
    internal var blurBackgroundCustomView: BlurBackgroundCustomView? = null
    internal var blurRadius: SeekBar? = null
    internal var blurRadiusText: TextView? = null

    private val touchListener = object : View.OnTouchListener {
        var dx: Float = 0.toFloat()
        var dy: Float = 0.toFloat()
        override fun onTouch(v: View, event: MotionEvent): Boolean {
            val target = findViewById<View>(R.id.blur_frame)
            if (event.action == MotionEvent.ACTION_DOWN) {
                dx = target.x - event.rawX
                dy = target.y - event.rawY
            } else if (event.action == MotionEvent.ACTION_MOVE) {
                target.x = event.rawX + dx
                target.y = event.rawY + dy
            }
            return true
        }
    }

    private var slideUp: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blur_background)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName

        blurBackgroundClasses = BlurBackgroundClasses(context)
        addEditLinearLayout = findViewById(R.id.addEditLinearLayout)
        addEditBlurImage = findViewById(R.id.addEditBlurImage)
        animationView1 = findViewById(R.id.animationView1)
        animationView2 = findViewById(R.id.animationView2)
        animationView3 = findViewById(R.id.animationView3)
        animationView4 = findViewById(R.id.animationView4)
        animationView5 = findViewById(R.id.animationView5)


        animationView1?.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.rotate_slow))
        animationView2?.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.rotate_slow))
        animationView3?.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.rotate_slow))
        animationView4?.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.rotate_slow))
        animationView5?.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.rotate_slow))

        blurBackgroundCustomView = findViewById<View>(R.id.blur_view) as BlurBackgroundCustomView

        blurRadius = findViewById<View>(R.id.blur_radius) as SeekBar
        blurRadius?.progress = 10
        blurRadius?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                updateRadius()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
        blurRadiusText = findViewById<View>(R.id.blur_radius_val) as TextView
        updateRadius()
        findViewById<View>(R.id.drag).setOnTouchListener(touchListener)

        //addEditLinearLayout?.isDrawingCacheEnabled = true

    }


    private fun updateRadius() {
        blurBackgroundCustomView?.setBlurRadius(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, blurRadius!!.progress.toFloat(), resources.displayMetrics))
        blurRadiusText?.text = blurRadius?.progress.toString() + "dp"
    }

    fun doSlide(v: View) {
        val view = findViewById<View>(R.id.blur_frame)
        view.animate().translationYBy(((if (slideUp) -1 else 1) * view.height).toFloat()).setDuration(1000).start()
        slideUp = !slideUp
    }
    fun openPopup(v: View) {
        startActivityForResult(Intent(applicationContext, TransparentActivity::class.java), 1)
        /*val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle("Alert")
        alertDialog.setMessage("Alert message to be shown")
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                    blurBackgroundClasses?.unBlurBackground(addEditBlurImage, addEditLinearLayout)

                })
        alertDialog.show()
        blurBackgroundClasses?.blurBackground(addEditBlurImage, addEditLinearLayout)*/

    }



    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
