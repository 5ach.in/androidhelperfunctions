package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.*
import android.widget.*
import android.content.DialogInterface
import android.text.TextUtils
import android.util.TypedValue
import android.view.inputmethod.InputMethodManager


class AddAutoCompleteWordDialogueFragment : DialogFragment(), ItemOnStartDragListener {

    private var addCustomFieldDialogueFragmentMainView: View? = null
    private var mItemTouchHelper: ItemTouchHelper? = null
    private var addCustomFieldLinearLayout: LinearLayout? = null
    private var addCustomFieldAddButton: ImageButton? = null
    private var addCustomFieldCancelButton: Button? = null
    private var addCustomFieldSaveButton: Button? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        addCustomFieldDialogueFragmentMainView = inflater.inflate(R.layout.add_auto_complete_word_dialogue_fragment, container, false)
        this.dialog.setCanceledOnTouchOutside(false)
        addCustomFieldLinearLayout = addCustomFieldDialogueFragmentMainView?.findViewById(R.id.addCustomFieldLinearLayout)




        var autoCompleteWordsCount = context!!.getSharedPreferences("AutoCompleteWords", Context.MODE_PRIVATE).getInt("autoCompleteWordsCount", 0)
        for (i in 0 until autoCompleteWordsCount) {
            val params = LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 15, 0, 15)
            val editTextToInflate = EditText(context)
            editTextToInflate.transformationMethod = null
            editTextToInflate.setTextSize(TypedValue.COMPLEX_UNIT_PX, 60.toFloat())
            editTextToInflate.background = null
            editTextToInflate.maxLines = 1
            editTextToInflate.canScrollHorizontally(2)
            editTextToInflate.ellipsize = TextUtils.TruncateAt.END
            editTextToInflate.setText(context!!.getSharedPreferences("AutoCompleteWords", Context.MODE_PRIVATE).getString("autoCompleteWords${i+1}", ""))
            /* USE THIS CODE FOR INFLATING A CUSTOM VIEW
            val layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val inflatedTicket = layoutInflater.inflate(R.layout.custom_view_you_created, null)
            var button: Button = inflatedTicket.findViewById(R.id.button_inside_the_custom_view)
            button.setText(listOfActivities.get(i).javaClass.simpleName)
            */
            editTextToInflate.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if(keyCode == KeyEvent.KEYCODE_DEL && editTextToInflate.text.isEmpty()) {
                    var parent: ViewGroup = editTextToInflate!!.parent as ViewGroup
                    parent.removeView(editTextToInflate)
                    if(addCustomFieldLinearLayout!!.childCount>0){
                        (addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).requestFocus()
                        (addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).setSelection((addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).text.length)
                    }
                    else{
                        var imm: InputMethodManager? = (context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        imm!!.hideSoftInputFromWindow(editTextToInflate.windowToken, 0)
                    }
                }
                false
            })
            addCustomFieldLinearLayout?.addView(editTextToInflate, addCustomFieldLinearLayout!!.childCount, params)

        }














        /*
        root = activity.getWindow().decorView.findViewById<View>(android.R.id.content)
        root.getViewTreeObserver().addOnGlobalLayoutListener(ViewTreeObserver.OnGlobalLayoutListener {
            if (root is ViewGroup) {
                findAllEdittexts(root as ViewGroup)
            }
        })*/
        addCustomFieldAddButton = addCustomFieldDialogueFragmentMainView?.findViewById(R.id.addCustomFieldAddButton)
        addCustomFieldAddButton?.setOnClickListener(View.OnClickListener {
            addWord()
        })


        addCustomFieldCancelButton = addCustomFieldDialogueFragmentMainView?.findViewById(R.id.addCustomFieldCancelButton)
        addCustomFieldCancelButton?.setOnClickListener(View.OnClickListener {
            dismiss()
        })
        addCustomFieldSaveButton = addCustomFieldDialogueFragmentMainView?.findViewById(R.id.addCustomFieldSaveButton)
        addCustomFieldSaveButton?.setOnClickListener(View.OnClickListener {
            saveCustomFieldDialogueFragment()
            dismiss()
        })

        return addCustomFieldDialogueFragmentMainView
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        mItemTouchHelper!!.startDrag(viewHolder)
    }


    fun addWord() {
        if(addCustomFieldLinearLayout!!.childCount>0){
            if(!((addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).text.toString().trim().equals(""))){
                val params = LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                params.setMargins(0, 15, 0, 15)
                val editTextToInflate = EditText(context)
                editTextToInflate.transformationMethod = null
                editTextToInflate.setTextSize(TypedValue.COMPLEX_UNIT_PX, 60.toFloat())
                editTextToInflate.background = null
                editTextToInflate.maxLines = 1
                editTextToInflate.canScrollHorizontally(2)
                editTextToInflate.ellipsize = TextUtils.TruncateAt.END
                editTextToInflate.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                    if(keyCode == KeyEvent.KEYCODE_DEL && editTextToInflate.text.isEmpty()) {
                        var parent: ViewGroup = editTextToInflate!!.parent as ViewGroup
                        parent.removeView(editTextToInflate)
                        if(addCustomFieldLinearLayout!!.childCount>0){
                            (addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).requestFocus()
                            (addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).setSelection((addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).text.length)
                        }
                        else{
                            var imm: InputMethodManager? = (context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                            imm!!.hideSoftInputFromWindow(editTextToInflate.windowToken, 0)
                        }
                    }
                    false
                })
                addCustomFieldLinearLayout?.addView(editTextToInflate, addCustomFieldLinearLayout!!.childCount, params)
                editTextToInflate.requestFocus()
            }
        }
        else{
            val params = LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 15, 0, 15)
            val editTextToInflate = EditText(context)
            editTextToInflate.transformationMethod = null
            editTextToInflate.setTextSize(TypedValue.COMPLEX_UNIT_PX, 60.toFloat())
            editTextToInflate.background = null
            editTextToInflate.maxLines = 1
            editTextToInflate.canScrollHorizontally(2)
            editTextToInflate.ellipsize = TextUtils.TruncateAt.END
            editTextToInflate.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if(keyCode == KeyEvent.KEYCODE_DEL && editTextToInflate.text.isEmpty()) {
                    var parent: ViewGroup = editTextToInflate!!.parent as ViewGroup
                    parent.removeView(editTextToInflate)
                    if(addCustomFieldLinearLayout!!.childCount>0){
                        (addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).requestFocus()
                        (addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).setSelection((addCustomFieldLinearLayout?.getChildAt(addCustomFieldLinearLayout!!.childCount-1) as EditText).text.length)
                    }
                    else{
                        var imm: InputMethodManager? = (context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        imm!!.hideSoftInputFromWindow(editTextToInflate.windowToken, 0)
                    }
                }
                false
            })
            addCustomFieldLinearLayout?.addView(editTextToInflate, addCustomFieldLinearLayout!!.childCount, params)
            editTextToInflate.requestFocus()
        }

        var imm: InputMethodManager? = (context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }


    fun saveCustomFieldDialogueFragment() {
        context!!.getSharedPreferences("AutoCompleteWords", Context.MODE_PRIVATE).edit().putInt("autoCompleteWordsCount", addCustomFieldLinearLayout!!.childCount).commit()
        for (i in 0 until addCustomFieldLinearLayout!!.childCount) {
            context!!.getSharedPreferences("AutoCompleteWords", Context.MODE_PRIVATE).edit().putString("autoCompleteWords${i+1}", (addCustomFieldLinearLayout?.getChildAt(i) as EditText).text.toString().trim()).commit()
        }
        dismiss()
        activity?.finish()
        startActivity(activity?.intent)

    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        dismiss()
    }
}