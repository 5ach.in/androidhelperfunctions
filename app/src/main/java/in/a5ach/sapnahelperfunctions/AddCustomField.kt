package `in`.a5ach.sapnahelperfunctions

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.*
import android.widget.Toast
import android.content.Context
import android.content.Intent
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import android.app.Activity




class AddCustomField : AppCompatActivity(), ItemOnStartDragListener {
    private var mItemTouchHelper: ItemTouchHelper? = null
    private var recyclerView: RecyclerView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_custom_field)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val adapter = RecyclerListAdapter(this, this)
        recyclerView = findViewById(R.id.addCustomFieldRecyclerView)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter = adapter

        val spanCount = resources.getInteger(R.integer.grid_columns)
        val layoutManager = GridLayoutManager(this, spanCount)
        recyclerView?.layoutManager = layoutManager
        val callback = ItemSimpleItemTouchHelperCallback(adapter)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper?.attachToRecyclerView(recyclerView)
        /*
        recyclerView.layoutManager = LinearLayoutManager(this)
        val callback = ItemSimpleItemTouchHelperCallback(adapter)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper!!.attachToRecyclerView(recyclerView)
        */

    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        mItemTouchHelper!!.startDrag(viewHolder)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.getItemId()) {
            R.id.save -> {
                var customFieldsCount = (getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getInt("customFieldsCount", 0))+1
                for (i in 0 until recyclerView!!.childCount) {
                    if((recyclerView?.getChildAt(i) as ViewGroup).findViewById<ImageButton>(R.id.customFieldInclude).visibility == View.VISIBLE){
                        var fieldName = (recyclerView?.getChildAt(i) as ViewGroup).findViewById<TextView>(R.id.text).text.toString()
                        getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldPositionOf${fieldName}${customFieldsCount}", i+1).commit()

                        if((recyclerView?.getChildAt(i) as ViewGroup).findViewById<TextView>(R.id.customFieldSwitch).visibility == View.VISIBLE){

                        }





                        getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldDefaultOf${fieldName}${customFieldsCount}", i+1).commit()
                    }
                }
                var addCustomFieldTitle: EditText? = findViewById(R.id.addCustomFieldTitle)
                getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putString("customFieldTitle${customFieldsCount}", addCustomFieldTitle?.text.toString()).commit()
                getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldsCount", customFieldsCount).commit()
                Toast.makeText(this, "Added custom field", Toast.LENGTH_SHORT).show()
                val returnIntent = Intent()
                setResult(Activity.RESULT_CANCELED, returnIntent)
                finish()
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.save_menu, menu)
        return true
    }
}