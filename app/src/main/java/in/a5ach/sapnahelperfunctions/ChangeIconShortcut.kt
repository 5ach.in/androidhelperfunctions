package `in`.a5ach.sapnahelperfunctions

import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.R.attr.name
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.pm.ResolveInfo
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.provider.MediaStore
import android.support.v4.content.pm.ShortcutInfoCompat
import android.support.v4.content.pm.ShortcutManagerCompat
import android.support.v4.graphics.drawable.IconCompat
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import android.widget.*
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.*


class ChangeIconShortcut : AppCompatActivity() {
    var context: Context = this

    var customShortcutImage: ImageButton? = null
    var customShortcutName: TextView? = null
    var customShortcutButton: Button? = null

    var SELECT_FILE: Int = 0
    private var permissionRequester: PermissionRequester? = null


    var icons = mutableMapOf<String, Int>()
    var dummy: TextView? = null
    var launcherActivityFullName: String = ""
    var launcherActivityName: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_icon_custom_shortcut)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName



        customShortcutImage = findViewById(R.id.customShortcutImage)
        customShortcutName = findViewById(R.id.customShortcutName)
        customShortcutButton = findViewById(R.id.customShortcutButton)
        customShortcutName?.text = "${applicationInfo.loadLabel(packageManager)}"



        var iconSet = (getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).getString("Icon", "Default"))

        icons.put("", R.mipmap.ic_launcher_round)

        var numberOfIcons = (getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).getInt("NumberOfIcons", 0))

        for (i in 1 until numberOfIcons+1) {
            var iconImageId = (getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).getInt("IconImageId${i}", 0))
            var iconImageName = (getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).getString("IconImageName${i}", ""))
            icons.put(iconImageName, iconImageId)

            if(iconSet.equals(iconImageName)){
                customShortcutImage?.setImageResource(iconImageId)
            }

        }

        dummy = findViewById(R.id.dummy)

        val linearListOfButtons: LinearLayout = findViewById(R.id.linearListOfButtons)
        for (entry in icons) {
            val params = LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 15, 0, 15)
            val buttonToInflate = ImageButton(this)
            //buttonToInflate.text = entry.key
            buttonToInflate.setImageResource(entry.value)

            val typedValue = TypedValue()
            theme.resolveAttribute(R.attr.selectableItemBackground, typedValue, true)
            buttonToInflate.setBackgroundResource(typedValue.resourceId)
            /* USE THIS CODE FOR INFLATING A CUSTOM VIEW
            val layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val inflatedTicket = layoutInflater.inflate(R.layout.custom_view_you_created, null)
            var button: Button = inflatedTicket.findViewById(R.id.button_inside_the_custom_view)
            button.setText(listOfActivities.get(i).javaClass.simpleName)
            */
            linearListOfButtons.addView(buttonToInflate, linearListOfButtons.childCount, params)

            buttonToInflate.setOnClickListener {
                getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).edit().putString("Icon", entry.key).commit()
                iconSet = entry.key
                changeIcon(entry.key)
            }
            buttonToInflate.setOnLongClickListener {
                Toast.makeText(this, "${entry.key}", Toast.LENGTH_LONG).show()
                true
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }


    fun changeIcon(iconName: String?) {
        //Toast.makeText(this, "${iconName}", Toast.LENGTH_LONG).show()
        //dummy?.text = "${dummy?.text}\nCubbidy"
        launcherActivityFullName = (getSharedPreferences("ChangeIconShortcut", Context.MODE_PRIVATE).getString("launcherActivityFullName", ""))
        for (entry in icons) {
            //dummy?.text = "${dummy?.text}\n${launcherActivityFullName}${entry.key}"
            if(entry.key.equals(iconName)){
                packageManager.setComponentEnabledSetting(
                        ComponentName(packageName, "${launcherActivityFullName}${entry.key}"),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP)
            }
            else{
                if(entry.key.equals("") && "dev".toLowerCase().equals((getSharedPreferences("MainActivity", Context.MODE_PRIVATE).getString("Dev", "")).toLowerCase())){

                }
                else{
                    packageManager.setComponentEnabledSetting(
                            ComponentName(packageName, "${launcherActivityFullName}${entry.key}"),
                            PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP)
                }
            }
        }
        /*android.os.Process.killProcess(android.os.Process.myPid())*/

        val homeIntent = Intent(Intent.ACTION_MAIN)
        homeIntent.addCategory( Intent.CATEGORY_HOME )
        homeIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(homeIntent)
    }


    fun createShortCut(shortcutName: String, shortcutIcon: Bitmap){
        val shortcutIntent = Intent(applicationContext, SplashScreen::class.java)
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        shortcutIntent.action = Intent.ACTION_MAIN

        val shortcut = ShortcutInfoCompat.Builder(applicationContext, "shortcut")
                .setShortLabel(shortcutName)
                .setIcon(IconCompat.createWithBitmap(shortcutIcon))
                .setIntent(shortcutIntent)
                .build()

        /* var shortcutManager = getSystemService(ShortcutManager::class.java)
         var shortcut2 = ShortcutInfo.Builder(this, "second_shortcut")
         shortcut2.setShortLabel(getString(R.string.app_name))
         shortcut2.setLongLabel(getString(R.string.app_name))
         shortcut2.setIcon(Icon.createWithResource(this, R.mipmap.ic_launcher))
         shortcut2.setIntent(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.co.in")))
         var shortcut3 = shortcut2.build()
         val numbers: MutableList<ShortcutInfo> = mutableListOf(shortcut3 as ShortcutInfo)
         val readOnlyView: List<ShortcutInfo> = numbers
         shortcutManager.dynamicShortcuts = readOnlyView*/

        ShortcutManagerCompat.requestPinShortcut(applicationContext, shortcut, null)
    }


    fun openGalary(view: View){
        permissionRequester = PermissionRequester(context, resources.getStringArray(R.array.permission_names)[25 - 1], resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[25 - 1]}_REQUEST_CODE", "integer", packageName)))
        permissionRequester!!.requestPermission()
    }
    fun createShortCut(view: View){
        var customShortcutImageResource: Drawable = (customShortcutImage!!.drawable)
        val bitmap = (customShortcutImageResource as BitmapDrawable).bitmap
        createShortCut(customShortcutName!!.text.toString(), bitmap)
        moveTaskToBack(true)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            (resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[25 - 1]}_REQUEST_CODE", "integer", packageName))) -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    intent.type = "image/*"
                    startActivityForResult(intent, SELECT_FILE)
                } else if (grantResults.isNotEmpty()) {
                    permissionRequester?.showSnackBar()
                }
                return
            }
        }
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
             if (requestCode == SELECT_FILE) {
                 var imageStream: InputStream? = null
                 try {
                     imageStream = contentResolver.openInputStream(data!!.data)
                 } catch (e: FileNotFoundException) {
                     e.printStackTrace()
                 }
                 var selectedImage: Bitmap = BitmapFactory.decodeStream(imageStream)
                 selectedImage = getResizedBitmap(selectedImage, 400)// 400 is for example, replace with desired size
                 customShortcutImage?.setImageBitmap(selectedImage)
            }
        }
    }



    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.width
        var height = image.height
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }
}
