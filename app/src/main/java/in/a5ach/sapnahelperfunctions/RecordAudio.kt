package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Chronometer
import android.widget.TextView
import android.widget.Toast
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat

class RecordAudio : AppCompatActivity() {

    var context: Context = this

    private var permissionRequester: PermissionRequester? = null


    internal var status: TextView? = null
    internal var outputFile: String? = null
    internal var myRecorder: MediaRecorder? = null
    internal var myPlayer: MediaPlayer? = null
    internal var starts: Button? = null
    internal var stopsrec:Button? = null
    internal var plays:Button? = null
    internal var stops:Button? = null
    internal var dateFormat: SimpleDateFormat? = null
    internal var currentTimeStamp: String? = null
    internal var mFileName: String? = null
    internal var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_audio)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = javaClass.simpleName


        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        this.supportActionBar

        status = findViewById<View>(R.id.seconds) as TextView
        starts = findViewById<View>(R.id.btnStart) as Button
        stopsrec = findViewById<View>(R.id.btnFinish) as Button
        //listings();
        plays = findViewById<View>(R.id.btnPlay) as Button
        stops = findViewById<View>(R.id.btnStop) as Button


        //starts?.setBackgroundColor(Color.RED)


        permissionRequester = PermissionRequester(context, resources.getStringArray(R.array.permission_names)[8], resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[8]}_REQUEST_CODE", "integer", packageName)))
        permissionRequester!!.requestPermission()

        /*requestPermissionClass = RequestPermissionClass(this, REQUEST_RECORD_AUDIO_PERMISSION_CODE, findViewById<View>(R.id.recordAudioLayout), REQUEST_RECORD_AUDIO_PERMISSION_STRING)
        if (requestPermissionClass.checkPermission() === true) {
            starts?.setBackgroundColor(Color.GREEN)
        }*/

        val chronometer = findViewById<View>(R.id.chronometer) as Chronometer // initiate a chronometer

        ///
        starts?.setOnClickListener { v ->
            // boolean mStartRecording = true;
            chronometer.base = SystemClock.elapsedRealtime()
            startRecording(v)
            chronometer.start()
        }


        stopsrec?.setOnClickListener { v ->
            chronometer.stop()
            stopRecording(v)
        }

        stops?.setOnClickListener { v -> stopPlaying(v) }

        plays?.setOnClickListener { v -> startPlaying(v) }

    }

    protected fun stopRecording(v: View) {
        try {
            myRecorder?.stop()
            myRecorder?.reset()
            myRecorder?.release()

            myRecorder = null

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: RuntimeException) {
            // no valid audio/video data has been received
            e.printStackTrace()
        }

        stopsrec?.isEnabled = false
        starts?.isEnabled = true
        //plays.setEnabled(true);
        status?.text = "Recording Stopped"
        status?.setTextColor(Color.RED)
        plays?.isEnabled = true

        //Toast.makeText(getApplicationContext(), "Recording Stopped", Toast.LENGTH_SHORT).show();
        //refresh();
    }

    fun refresh() {
        val intent = intent
        finish()
        startActivity(intent)
    }

    protected fun startRecording(v: View) {
        mFileName = externalCacheDir!!.absolutePath + "/recordings"

        val folder = File(externalCacheDir!!.absolutePath + "/recordings")
        var success = true
        if (!folder.exists()) {
            success = folder.mkdir()
        }
        if (success) {
            folder.mkdirs()
        } else {
            // Do something else on failure
        }
        val file = File(mFileName)
        val list = file.listFiles()
        for (f in list!!) {
            count++
        }
        Toast.makeText(this, "there are $count files in $mFileName", Toast.LENGTH_SHORT).show()


        outputFile = externalCacheDir!!.absolutePath + "/recordings" + "/" + count + "records.mp3"

        //outputFile = mFileName + count + ".mp3";


        //Toast.makeText(getApplicationContext(), outputFile, Toast.LENGTH_LONG).show();
        myRecorder = MediaRecorder()
        myRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
        myRecorder?.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        myRecorder?.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
        myRecorder?.setOutputFile(outputFile)


        try {
            myRecorder?.prepare()
            myRecorder?.start()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        //plays.setEnabled(false);
        stopsrec?.isEnabled = true
        starts?.isEnabled = false
        status?.text = "Recording"
        status?.setTextColor(Color.GREEN)
        //Toast.makeText(getApplicationContext(), "Recording...", Toast.LENGTH_SHORT).show();
    }

    public override fun onPause() {
        super.onPause()
        if (myRecorder != null) {
            myRecorder?.release()
            myRecorder = null
        }

        /* if (myPlayer != null) {
            myPlayer.release();
            myPlayer = null;
        }*/
    }


    private fun startPlaying(v: View) {
        myPlayer = MediaPlayer()
        try {
            myPlayer?.setDataSource(outputFile)
            myPlayer?.prepare()
            myPlayer?.start()
            Toast.makeText(this, outputFile, Toast.LENGTH_SHORT).show()


        } catch (e: IOException) {
        }

        stops?.isEnabled = true

    }

    private fun stopPlaying(v: View) {
        myPlayer?.release()
        myPlayer = null
        plays?.isEnabled = true

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        //final MenuItem menuItem = menu.add(Menu.NONE, 1000, Menu.NONE, R.string.action_save);
        //MenuItemCompat.setShowAsAction(menuItem, MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menuInflater.inflate(R.menu.save_menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.save) {
            Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show()
            return true
        }

        when (item.itemId) {
            // Respond to the action bar's Up/Home/back button
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            (resources.getInteger(resources.getIdentifier("${resources.getStringArray(R.array.permission_names)[8]}_REQUEST_CODE", "integer", packageName))) -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    starts?.setBackgroundColor(Color.GREEN)
                } else if (grantResults.isNotEmpty()) {
                    permissionRequester?.showSnackBar()
                    starts?.setBackgroundColor(Color.RED)
                }
                return
            }
        }
    }

}