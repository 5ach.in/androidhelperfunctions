package `in`.a5ach.sapnahelperfunctions

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.*
import android.widget.*
import android.content.DialogInterface


class AddCustomFieldDialogueFragment : DialogFragment(), ItemOnStartDragListener {

    private var addCustomFieldDialogueFragmentMainView: View? = null
    private var mItemTouchHelper: ItemTouchHelper? = null
    private var recyclerView: RecyclerView? = null
    private var addCustomFieldCancelButton: Button? = null
    private var addCustomFieldSaveButton: Button? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        addCustomFieldDialogueFragmentMainView = inflater.inflate(R.layout.add_custom_field_dialogue_fragment, container, false)
        this.dialog.setCanceledOnTouchOutside(false)/*
        root = activity.getWindow().decorView.findViewById<View>(android.R.id.content)
        root.getViewTreeObserver().addOnGlobalLayoutListener(ViewTreeObserver.OnGlobalLayoutListener {
            if (root is ViewGroup) {
                findAllEdittexts(root as ViewGroup)
            }
        })*/


        val adapter = RecyclerListAdapter(context, this)
        recyclerView = addCustomFieldDialogueFragmentMainView?.findViewById(R.id.addCustomFieldRecyclerView)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter = adapter


        val spanCount = resources.getInteger(R.integer.grid_columns)
        val layoutManager = GridLayoutManager(context, spanCount)
        recyclerView?.layoutManager = layoutManager
        val callback = ItemSimpleItemTouchHelperCallback(adapter)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper?.attachToRecyclerView(recyclerView)


        addCustomFieldCancelButton = addCustomFieldDialogueFragmentMainView?.findViewById(R.id.addCustomFieldCancelButton)
        addCustomFieldCancelButton?.setOnClickListener(View.OnClickListener {
            dismiss()
        })
        addCustomFieldSaveButton = addCustomFieldDialogueFragmentMainView?.findViewById(R.id.addCustomFieldSaveButton)
        addCustomFieldSaveButton?.setOnClickListener(View.OnClickListener {
            saveCustomFieldDialogueFragment()
            dismiss()
        })

        return addCustomFieldDialogueFragmentMainView
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        mItemTouchHelper!!.startDrag(viewHolder)
    }


    fun saveCustomFieldDialogueFragment() {
        var customFieldsCount = (context!!.getSharedPreferences("CustomFields", Context.MODE_PRIVATE).getInt("customFieldsCount", 0))+1
        for (i in 0 until recyclerView!!.childCount) {
            if((recyclerView?.getChildAt(i) as ViewGroup).findViewById<ImageButton>(R.id.customFieldInclude).visibility == View.VISIBLE){
                var fieldName = (recyclerView?.getChildAt(i) as ViewGroup).findViewById<TextView>(R.id.text).text.toString()
                context!!.getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldPositionOf${fieldName}${customFieldsCount}", i+1).commit()



               // Toast.makeText(this, seekBarView.javaClass.getName().toString(), Toast.LENGTH_SHORT).show()


                if(fieldName.toLowerCase().equals("switch")){
                    var isSwitchChecked = (recyclerView?.getChildAt(i) as ViewGroup).findViewById<Switch>(R.id.customFieldSwitch).isChecked
                    if(isSwitchChecked){
                        context!!.getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldValueOf${fieldName}${customFieldsCount}", 1).commit()
                    }
                    else{
                        context!!.getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldValueOf${fieldName}${customFieldsCount}", 0).commit()
                    }
                }
                else if(fieldName.toLowerCase().equals("seekbar")){
                    var seekbarView = (recyclerView?.getChildAt(i) as ViewGroup).findViewById<SeekBar>(R.id.customFieldSeekBar)
                    context!!.getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldValueOf${fieldName}${customFieldsCount}", seekbarView.progress).commit()
                }
                else if(fieldName.toLowerCase().equals("edittext")){
                    var edittextView = (recyclerView?.getChildAt(i) as ViewGroup).findViewById<EditText>(R.id.customFieldEditText)
                    context!!.getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putString("customFieldValueOf${fieldName}${customFieldsCount}", edittextView.text.toString()).commit()
                }



                if((recyclerView?.getChildAt(i) as ViewGroup).findViewById<TextView>(R.id.customFieldSwitch).visibility == View.VISIBLE){

                }
                context!!.getSharedPreferences("CustomFields", Context.MODE_PRIVATE).edit().putInt("customFieldDefaultOf${fieldName}${customFieldsCount}", i+1).commit()
            }
        }
        var addCustomFieldTitle: EditText = addCustomFieldDialogueFragmentMainView!!.findViewById(R.id.addCustomFieldTitle)
        context!!.getSharedPreferences("CustomFields", Context.MODE_PRIVATE)?.edit()?.putString("customFieldTitle${customFieldsCount}", addCustomFieldTitle?.text.toString())?.commit()
        context!!.getSharedPreferences("CustomFields", Context.MODE_PRIVATE)?.edit()?.putInt("customFieldsCount", customFieldsCount)?.commit()
        Toast.makeText(context, "Added custom field", Toast.LENGTH_SHORT).show()
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        activity?.recreate()
    }
}