package in.a5ach.sapnahelperfunctions;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Simple RecyclerView.Adapter that implements {@link ItemTouchHelperAdapter} to respond to move and
 * dismiss events from a {@link android.support.v7.widget.helper.ItemTouchHelper}.
 *
 * @author Paul Burke (ipaulpro)
 */
public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {

    private Context ctx;

    private final List<String> mItems = new ArrayList<>();

    private final ItemOnStartDragListener mDragStartListener;

    public RecyclerListAdapter(Context context, ItemOnStartDragListener dragStartListener) {
        ctx = context;
        mDragStartListener = dragStartListener;
        mItems.addAll(Arrays.asList(context.getResources().getStringArray(R.array.custom_view_items)));
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {


        if(mItems.get(position).toLowerCase().equals("switch")){
            holder.customFieldSwitch.setVisibility(View.VISIBLE);
        }
        else if(mItems.get(position).toLowerCase().equals("edittext")){
            holder.customFieldEditText.setVisibility(View.VISIBLE);
        }
        else if(mItems.get(position).toLowerCase().equals("seekbar")){
            holder.customFieldSeekBar.setVisibility(View.VISIBLE);
        }

        holder.textView.setText(mItems.get(position));



        holder.customFieldDontInclude.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.customFieldInclude.getVisibility() == View.GONE){
                    holder.customFieldInclude.setVisibility(View.VISIBLE);
                    holder.customFieldDontInclude.setVisibility(View.GONE);
                }
            }
        });
        holder.customFieldInclude.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.customFieldDontInclude.getVisibility() == View.GONE){
                    holder.customFieldDontInclude.setVisibility(View.VISIBLE);
                    holder.customFieldInclude.setVisibility(View.GONE);
                }
            }
        });

        // Start a drag whenever the handle view it touched
        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(holder);
                }
                return false;
            }
        });
    }

    @Override
    public void onItemDismiss(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mItems, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    /**
     * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
     * "handle" view that initiates a drag event when touched.
     */
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {

        public final TextView textView;
        public final Switch customFieldSwitch;
        public final EditText customFieldEditText;
        public final SeekBar customFieldSeekBar;
        public final ImageButton customFieldDontInclude;
        public final ImageButton customFieldInclude;

        public ItemViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text);
            customFieldSwitch = itemView.findViewById(R.id.customFieldSwitch);
            customFieldEditText = itemView.findViewById(R.id.customFieldEditText);
            customFieldSeekBar = itemView.findViewById(R.id.customFieldSeekBar);
            customFieldDontInclude = itemView.findViewById(R.id.customFieldDontInclude);
            customFieldInclude = itemView.findViewById(R.id.customFieldInclude);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }
}